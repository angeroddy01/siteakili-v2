<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Akili</title>
    <!-- favicons Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/AKILI_Logo Officiel_Plan de travail 1.jpg" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/AKILI_Logo Officiel_Plan de travail 1.jpg" />
    <link rel="manifest" href="assets/images/favicons/site.webmanifest" />


    <link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css?version=2" />
    <style>
        <?php include "assets/css/maintenance.css" ?>
    </style>
</head>

<body>
    <div class="topbar mb-4">
        <img class="akili-log" src="assets/images/AKILI_Logo Officiel-02.jpg">
        <div class="languages-log">
            <a href="french-index.php"><img src="assets/images/french.png"></a>
            <a href="index.php"><img src="assets/images/english.png"></a>
        </div>
    </div>
    <section class="maintenance">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-content">
                    <h1 class="text-center">A new look is coming soon...</h1>
                    <p>Stay tuned</p>
                    <div class="network d-flex mb-5">
                        <a href="https://www.facebook.com/mqashci/">
                            <img src="assets/images/fb_logo.png">
                        </a>
                        <a href="https://www.linkedin.com/company/mqash/about/">
                            <img src="assets/images/in.png">
                        </a>
                        <a href="">
                            <img src="assets/images/insta.png">
                        </a>
                        <a href="https://twitter.com/MQashCI">
                            <img src="assets/images/twity.png">
                        </a>
                    </div>
                    <img src="assets/images/maint-img.png">
                </div>
            </div>
        </div>
    </section>

    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>