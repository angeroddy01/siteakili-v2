<!DOCTYPE html>
<html lang="en">

<!-- Head Start -->
<?php include("assets/includes/head.php");  ?>
<!-- Head Fin -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header Start -->
        <?php include("assets/includes/header.php")  ?>
        <!-- Header Fin -->
        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->

        <!--Page Header Start-->
        <section class="page-header">
            <div class="page-header__bg"></div><!-- /.page-header__bg -->
            <div class="page-header-shape-1"></div><!-- /.page-header-shape-1 -->
            <div class="page-header-shape-2"></div><!-- /.page-header-shape-2 -->
            <div class="page-header-shape-3"></div><!-- /.page-header-shape-3 -->
            <div class="container">
                <div class="page-header__inner">
                    <h2>Conseil & Stratégies </h2>
                </div>
            </div>
        </section>
        <!--Page Header End-->

        <!--Cases One Start-->
        <section class="cases-one cases-page">
            <div class="container">
                <div class="row text-center">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat architecto possimus aspernatur odit hic maiores libero accusantium consequatur laudantium voluptate sapiente deserunt iste, officia quam at alias recusandae cum delectus.
                        Ipsam eligendi quisquam blanditiis excepturi! Impedit neque accusantium dolor iure deleniti distinctio! Commodi unde rem deserunt repellat reiciendis suscipit dolorem doloribus facilis dolorum vero cumque necessitatibus, sequi fugiat eius enim?
                        Eum, eos? Harum nemo beatae earum amet nam dignissimos aperiam est quaerat quia. Porro, facilis. Distinctio repudiandae tempora fugit expedita ipsam similique possimus magnam est ab, nihil eveniet atque incidunt.</p>
                    <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!--Cases One Single-->
                        <div class="cases-one__single">
                            <div class="cases-one__img-box">
                                <div class="cases-one__img">
                                    <img src="assets/images/resources/cases-page-img-1.jpg" alt="">
                                </div>
                                <div class="cases-one__content">
                                    <div class="cases-one__icon">
                                        <span class="icon-mobile-analytics"></span>
                                    </div>
                                    <p class="cases-one__tagline">Thought leadership</p>
                                    <h2 class="cases-one__tilte"><a href="cases-details.html">businesses growth</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <!--Cases One Single-->
                        <div class="cases-one__single">
                            <div class="cases-one__img-box">
                                <div class="cases-one__img">
                                    <img src="assets/images/resources/cases-page-img-2.jpg" alt="">
                                </div>
                                <div class="cases-one__content">
                                    <div class="cases-one__icon">
                                        <span class="icon-research"></span>
                                    </div>
                                    <p class="cases-one__tagline">Risk management</p>
                                    <h2 class="cases-one__tilte"><a href="cases-details.html">Marketing advice</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                        <!--Cases One Single-->
                        <div class="cases-one__single">
                            <div class="cases-one__img-box">
                                <div class="cases-one__img">
                                    <img src="assets/images/resources/cases-page-img-3.jpg" alt="">
                                </div>
                                <div class="cases-one__content">
                                    <div class="cases-one__icon">
                                        <span class="icon-creative"></span>
                                    </div>
                                    <p class="cases-one__tagline">Business strategy</p>
                                    <h2 class="cases-one__tilte"><a href="cases-details.html">finance consulting</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Cases One End-->

        <!--Site Footer One Start-->
        <?php include("assets/includes/footer.php")    ?>
        <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

           <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>


    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>


    <!-- template js -->
    <script src="assets/js/aivons.js"></script>



</body>

</html>