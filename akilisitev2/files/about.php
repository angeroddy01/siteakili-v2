<!DOCTYPE html>
<html lang="fr">

<!-- HEAD DEBUT -->
<?php include("assets/includes/head.php"); ?>
<!-- HEAD FIN -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header DEBUT -->
        <?php include("assets/includes/header.php");   ?>
        <!-- Header FIN -->

        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <section class="main-slider main-slider-three">
            <div class="swiper-container thm-swiper__slider" data-swiper-options='{"slidesPerView": 1, "loop": true,
            "effect": "fade",
            "pagination": {
                "el": "#main-slider-pagination",
                "type": "bullets",
                "clickable": true
            },
            "navigation": {
                "nextEl": "#main-slider__swiper-button-next",
                "prevEl": "#main-slider__swiper-button-prev"
            },
            "autoplay": {
                "delay": 5000000
            }}'>
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="image-layer" id="image-layer">
                                <video playsinline autoplay muted loop poster="" id="myVideo">
                                    <source src="assets/videos/A propos de nous.mp4" type="video/webm">
                                </video>
                            </div>
                            <!-- /.image-layer -->
                            <div class="container">
                                <div class="row d-flex justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                            data-wow-duration="1500ms">
                                            <h3 class="text-center main-text">Découvrez Akili</h3>
                                            <h4 class="text-center mt-3 fw-bold">Entreprise de services numériques</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Page Header Start-->

        <!--Page Header End-->

        <!--About Start-->
        <section class="about panel" data-color="white">
            <div class=" container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="about__img-box">
                            <div class="about-img">
                                <img src="assets/images/Groupe 1@2x.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 mt-5">
                        <div class="about__right">
                            <h2 class="vision__title">Notre mission</h2>
                            
                             <p class="services-details__top-text">
                                     Notre mission est de fournir aux populations défavorisées et à celles vivant en milieu rural notamment, des services de conseils et d’ingénierie en matière de transformation numérique adaptés, abordables et accessibles à partir de leur mobile et ainsi, réduire la fracture sociale.
                                    
                                </p>
                           

                          

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--About End-->

        <!-- Vision Section -->

      
            <div class="container">
                 
                    <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="vision__right">
                            <h2 class="vision__title">Notre vision</h2>
                             <p class="services-details__top-text">
                                     À l’horizon 2025, être l’acteur de référence en Afrique subsaharienne francophone pour l’accompagnement des organisations dans la transformation afin de leur permettre d'être plus efficaces, compétitives, pérennes et impacter les vies des populations .
                                    
                                </p>
                           
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        
                            <div class="w-250">
                                <img src="assets/images/vision.jpeg" alt="">
                            </div>
                    </div>
                   </div
                  
            </div>
                
       
        <!-- Vision Section End -->


        </div>
         <div class="justify-content-center">
        <section class="valeurs panel  justify-content-center wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms"
            data-color="gray">
            <div class="container justify-content-center ">
                <div class="section-title text-center">
                    <h2 class="section-title__title">Nos valeurs</h2>
                </div>

                <div class="container justify-content-center">

                    <div class="row">

                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                        data-bs-target="#r" type="button" role="tab" aria-controls="home"
                                        aria-selected="true"><img src="assets/images/GroupR.png" alt="" class="ab"
                                            srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#e"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false"><img
                                            src="assets/images/GroupE.png" alt="" class="ab" srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#a"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false"><img
                                            src="assets/images/GroupA.png" alt="" class="ab" srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#c"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false"><img
                                            src="assets/images/GroupC.png" alt="" class="ab" srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#t"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false"><img
                                            src="assets/images/GroupT.png" alt="" class="ab" srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#i"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false"><img
                                            src="assets/images/GroupI.png" alt="" class="ab" srcset=""></button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#f"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false"><img
                                            src="assets/images/GroupF.png" alt="" class="ab" srcset=""></button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="r" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <img src="assets/images/GroupRr.png" alt="" class="ar" srcset="">
                                </div>
                                <div class="tab-pane fade" id="e" role="tabpanel" aria-labelledby="profile-tab"><img
                                        src="assets/images/GroupEr.png" alt="" class="ar" srcset=""></div>
                                <div class="tab-pane fade" id="a" role="tabpanel" aria-labelledby="contact-tab"><img
                                        src="assets/images/GroupAr.png" alt="" class="ar" srcset=""></div>
                                <div class="tab-pane fade" id="c" role="tabpanel" aria-labelledby="contact-tab"><img
                                        src="assets/images/GroupCr.png" alt="" class="ar" srcset=""></div>
                                <div class="tab-pane fade" id="t" role="tabpanel" aria-labelledby="contact-tab"><img
                                        src="assets/images/GroupTr.png" alt="" class="ar" srcset=""></div>
                                <div class="tab-pane fade" id="i" role="tabpanel" aria-labelledby="contact-tab"><img
                                        src="assets/images/GroupIr.png" alt="" class="ar" srcset=""></div>
                                <div class="tab-pane fade" id="f" role="tabpanel" aria-labelledby="contact-tab"><img
                                        src="assets/images/GroupFr.png" alt="" class="ar" srcset=""></div>
                            </div>
                        </div>

                    </div>

                </div>



            </div>
        </section></div>
         <section class="valeurs panel   wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms"
            data-color="gray">
            <div class="container justify-content-center ">
                <div class="section-title text-center">
                    <h2 class="section-title__title">Nos engagements</h2>
                </div>
                <section class="why-us">
		<div class="container">
	        <div class="row">
				<div class="col-md-8 offset-md-2">
					
					<p class="mb-5 text-center services-details__top-text">Dans la continuité de notre ambition d’être le leader dans nos métiers, nous entendons également jouer pleinement notre rôle dans la préservation de notre planète. À ce titre, nous œuvrons dès à présent et ce jusqu’à l’horizon 2025 à accentuer notre contribution autour de cinq (5) objectifs de développement durable 5 (ODD) qui constituent le véhicule de nos engagements :</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-lg-4">
					<div class="box">
						<span>01</span>
						<h4><a href="#">PAS DE PAUVRETE (ODD.1)</a></h4>
						<p class="fs-6 odd"> <b>Donner ce que l’on n’utilise pas </b> <br>
                           Chacun de nos collaborateurs est engagé au moins une fois l’an, à donner ce qu’il n’utilise pas. À ce titre nous avons initié une journée de collecte de biens de toutes natures (vêtements, nourriture, jouets, livres, manuels scolaires, etc …) en vue de les reverser à un orphelinat ou à un centre d’accueil. Cette campagne a lieu chaque année au cours du mois de septembre.
                        </p>
						<img src="assets/images/odd1.png" alt="">
					</div>
				</div>

				<div class="col-sm-6 col-lg-4">
					<div class="box">
						<span>02</span>
						<h4><a href="#">ÉGALITE ENTRE LES SEXES (ODD.5)</a></h4>
						<p class="fs-6 odd"><b>Autonomiser les femmes et les jeunes filles et défendez l’égalité de leurs droits</b> <br>
                        Dans le cadre de notre processus de recrutement et de mobilité en interne, nous offrons les mêmes chances à tous sans aucune forme de discrimination. Toutefois à compétence égale, une attention particulière sera portée aux candidatures féminines de manière à viser l’équilibre du genre au sein des effectifs de AKILI à l’horizon 2025.
                           </p>
						<img src="assets/images/odd2.png" alt="">
					</div>
				</div>

				<div class="col-sm-6 col-lg-4">
					<div class="box">
						<span>03</span>
						<h4><a href="#">TRAVAUX DECENTS <br>ET CROISSANCE ÉCONOMIQUE (ODD.8)</a></h4>
						<p class="fs-6 odd"> <b> Créer des opportunités d'emplois pour les jeunes</b> <br>
                         À travers la mise en place de partenariats <br> Écoles – Entreprise, nous voulons œuvrer à l’insertion des jeunes diplômés dans notre entreprise en leur offrant des opportunités de stage et de 1er emploi afin de contribuer à leur épanouissement social et en faire des acteurs de la croissance économique en Afrique.</p>
						<img src="assets/images/odd3.png" alt="">
					</div>
				</div>

				<div class="col-sm-6 col-lg-6">
					<div class="box">
						<span>04</span>
						<h4><a href="#">CONSOMMATION ET PRODUCTION RESPONSABLE (ODD.12)</a></h4>
						<p class="fs-6 odd"><b>Recycler le papier, le verre, le plastique et l'aluminium</b> <br>
                         Grace à la digitalisation de nos processus internes d’une part, et la mise en place de mesures visant à optimiser l’usage du papier par nos collaborateurs, nous avons l’ambition de réduire notre volume d’impression de 50% à l’horizon 2025. Nous veillons à la sensibilisation de l’ensemble de nos collaborateurs par tous les moyens ainsi que des parties avec qui nous interagissons sur la nécessité de la consommation responsable (du papier).</p>
						<img src="assets/images/odd4.png" alt="">
					</div>
				</div>

				<div class="col-sm-6 col-lg-6">
					<div class="box">
						<span>05</span>
						<h4><a href="#">VIE TERRESTRE (ODD.15)</a></h4>
						<p class="fs-6 odd"><b>Planter un arbre et protéger l'environnement</b> <br>
Nous veillons à initier ou à associer notre image à des campagnes de reboisements toutes les fois que cela est possible. Nous contribuons ainsi à accroître le boisement et la restauration du couvert végétal dégradé au niveau local.
</p>
						<img src="assets/images/odd5.png" alt="">
					</div>
				</div>

				
			</div>
		</div>
	</section>
	<!-- End Why Us Section -->



            </div>
        </section>

        </section>

        <!-- Vision Section End -->
        <!-- Nos valeurs -->
       
    <!-- Nos valeurs Fin --->
    <!--- Notre engagement social  -->
    <!-- <section class="engagement panel" data-color="white">
        <div class="container-fluid">
            <div class="row row-engagement" style="background: url(assets/images/eco2.jpg);">
                <div class="col-10 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <h1>Durabilité</h1>
                    <p class="services-details__top-text text-white  fs-3">
                                    Parce que le développement durable est plus qu’une nécessité pour préserver notre
                        environnement, AKILI est fermement engagée dans une démarche de durabilité à travers toutes ses
                        activités et dans l’accompagnement de ses clients au travers des pratiques résolument tournées
                        vers la construction d’une société résiliente et soucieuse de l’écologie.
                        AKILI s’engage à rendre accessible la technologie pour le bien-être des communautés.
                                    
                    </p>
                    
                </div>
            </div>
        </div>
    </section> -->
    <!--- Notre engagement social FIN   --->
    <!--Team One Start-->
    <section class="team-one panel" data-color="white" style="display: none;">
        <div class="team-one__container">
            <div class="section-title text-center">
                <h2 class="section-title__title"> Notre leadership</h2>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/Nadine.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Nadinne Ebelle Kotto</h3>
                                <p class="team-one__title">Fondatrice</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/kouakou.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Kouakou </h3>
                                <p class="team-one__title">Manager IT</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/max.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Hermann Max</h3>
                                <p class="team-one__title">Manager IT</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/test.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Myra Montgomery</h3>
                                <p class="team-one__title">Consultant</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contact Me</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/tap.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Nadinne Ebelle Kotto</h3>
                                <p class="team-one__title">Fondatrice</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/vieux.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Kouakou </h3>
                                <p class="team-one__title">Manager IT</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/kpaki.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Hermann Max</h3>
                                <p class="team-one__title">Manager IT</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contactez moi</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <!--Team One Single-->
                    <div class="team-one__single">
                        <div class="team-one__img">
                            <img src="assets/images/team/gracce.jpg" alt="">
                            <div class="team-one__hover-content">
                                <h3 class="team-one__name">Myra Montgomery</h3>
                                <p class="team-one__title">Consultant</p>
                            </div>
                            <div class="team-one__bottom">
                                <div class="team-one__btn-box">
                                    <a href="contact.html" class="team-one__btn">Contact Me</a>
                                </div>
                                <div class="team-one__social">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--Team One End-->

    <!--Site Footer One Start-->
    
    <?php include("assets/includes/footer.php");  ?>
    <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155"
                        alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> --><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>

    <!-- template js -->
    <script src="assets/js/aivons.js"></script>
    <!-- Changing background color -->
    <!--<script type="text/javascript">
        $(window).scroll(function() {

            // selectors
            var $window = $(window),
                $body = $('body'),
                $panel = $('.panel');

            // Change 33% earlier than scroll position so colour is there when you arrive.
            var scroll = $window.scrollTop() + ($window.height() / 3);

            $panel.each(function() {
                var $this = $(this);

                // if position is within range of this panel.
                // So position of (position of top of div <= scroll position) && (position of bottom of div > scroll position).
                // Remember we set the scroll to 33% earlier in scroll var.
                if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

                    // Remove all classes on body with color-
                    $body.removeClass(function(index, css) {
                        return (css.match(/(^|\s)color-\S+/g) || []).join(' ');
                    });

                    // Add class of currently active div
                    $body.addClass('color-' + $(this).data('color'));
                }
            });

        }).scroll();
    </script>-->
    <script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    </script>
</body>

</html>