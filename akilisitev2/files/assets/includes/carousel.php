
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>
<section id="slider">
  <div class="container">
   
	  <div class="slider">
				<div class="owl-carousel">
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4 p-3">
							<img src="assets/images/cas1.webp" />
						</div>
						<h5 class="mb-0 text-center"><b>Gestion de la file d’attente</b></h5>
						<h5 class="mb-0 text-center"><b>Gestion intelligente de la relation client en agence</b></h5>
						<p class="text-center p-4">la personnalisation  permet une meilleur prise en charge du client en agence et in fine permet d’améliorer son expérience.</p>
					</div>
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4  p-3">
							<img src="assets/images/cas2.webp">
						</div>
						<h5 class="mb-0 text-center"><b>Onboarding Client</b></h5>
						<h5 class="mb-0 text-center bnv"><b>Automatisation du contrôle homologation</b></h5>
						<p class="text-center p-4">Orange Bank Africa atteint une performance remarquable dans son processus de recrutement client grâce à l’intelligence artificielle.</p>
					</div>
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4 p-3">
							<img src="assets/images/cas3.webp">
						</div>
						<h5 class="mb-0 text-center "><b>Digitalisation des paiements </b></h5>
						<h5 class="mb-0 text-center"><b>dans les chaînes de valeur agricoles</b></h5>
						<p class="text-center p-4">Etude sur la digitalisation des paiements dans les chaînes de valeur agricoles et mise en place d'une stratégie d'introduction des moyens de paiements numériques.</p>
					</div>
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4 p-3">
							<img src="assets/images/cas1.webp" />
						</div>
						<h5 class="mb-0 text-center "><b>Gestion de la file d’attente</b></h5>
						<h5 class="mb-0 text-center"><b>Gestion intelligente de la relation client en agence</b></h5>
						<p class="text-center p-4">la personnalisation  permet une meilleur prise en charge du client en agence et in fine permet d’améliorer son expérience.</p>
					</div>
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4  p-3">
							<img src="assets/images/cas2.webp">
						</div>
						<h5 class="mb-0 text-center"><b>Onboarding Client</b></h5>
						<h5 class="mb-0 text-center"><b>Automatisation du contrôle homologation</b></h5>
						<p class="text-center p-4">Orange Bank Africa atteint une performance remarquable dans son processus de recrutement client grâce à l’intelligence artificielle.</p>
					</div>
					<div class="slider-card">
						<div class="d-flex justify-content-center align-items-center mb-4 p-3">
							<img src="assets/images/cas3.webp">
						</div>
						<h5 class="mb-0 text-center"><b>Digitalisation des paiements dans les chaînes de valeur agricoles</b></h5>
						<h5 class="mb-0 text-center"><b></b></h5>
						<p class="text-center p-4">Etude sur la digitalisation des paiements dans les chaînes de valeur agricoles et mise en place d'une stratégie d'introduction des moyens de paiements numériques.</p>
					</div>
					
					
					
				</div>
			</div>
  </div>
 
</section>

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script> 
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" ></script>
	<script src="assets/owl.carousel.min.js"></script>
	<script src="assets/script.js"></script>
	
</body>
</html>
