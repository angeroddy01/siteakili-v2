<div class="main-slider__content wow fadeInUp" data-wow-delay="300ms" data-wow-duration="4000ms">
    <h3 class="text-center main-text">Accompagner.</h3>
    <h4 class="mt-3">Votre transformation</h4>
    <div class="center-con">
        <div class="round ">
            <div id="cta">
                <span class="arrow primera next "></span>
                <span class="arrow segunda next "></span>

            </div>
        </div>
        <a href="about.php">
            <h5>En savoir plus</h5>
        </a>
    </div>
</div>