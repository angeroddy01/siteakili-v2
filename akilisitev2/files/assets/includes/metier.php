  <section class="real-world">
      <div class="real-world-shape wow slideInLeft" data-wow-delay="100ms" data-wow-duration="2500ms" style="background-image: url(assets/images/shapes/real-world-shape.png)"></div>
      <div class="container-fluid">
          <div class="section-title text-center">
              <h2 class="section-title__title">Notre Expertise métier </h2>
              <span class="section-title__tagline">Nous fournissons une gamme complète de services pour garantir votre progrès et anticiper vos besoins d'évolutions .</span>
          </div>
          <div class="row">
              <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                  <!--Real World Single-->
                  <div class="real-world__single">
                      <h2 class="real-world__title"><a href="conseil.php">Conseil & <br>stratégies</a>
                      </h2>
                      <a href="conseil.php" class="real-world__btn">En savoir plus</a>
                      <div class="real-world__icon-box">
                          <span class="icon-report"></span>
                      </div>
                  </div>
              </div>
              <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                  <!--Real World Single-->
                  <div class="real-world__single">
                      <h2 class="real-world__title"><a href="assistance.php">Assistance <br>technique </a></h2>
                      <a href="assistance.php" class="real-world__btn">En savoir plus</a>
                      <div class="real-world__icon-box">
                          <span class="icon-data-analytics"></span>
                      </div>
                  </div>
              </div>
              <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                  <!--Real World Single-->
                  <div class="real-world__single">
                      <h2 class="real-world__title"><a href="solutions.php">Implémentation<br>des solutions</a>
                      </h2>
                      <a href="solutions.php" class="real-world__btn">En savoir plus</a>
                      <div class="real-world__icon-box">
                          <span class="icon-software"></span>
                      </div>
                  </div>
              </div>
              <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                  <!--Real World Single-->
                  <div class="real-world__single">
                      <h2 class="real-world__title"><a href="solutions.php">Conduite<br>du changement</a></h2>
                      <a href="solutions.php" class="real-world__btn">En savoir plus</a>
                      <div class="real-world__icon-box">
                          <span class="icon-software"></span>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>