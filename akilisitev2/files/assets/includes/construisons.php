<section class="welcome-one">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="welcome-one__left">
                    <div class="welcome-one__img-box">
                        <div class="welcome-one__img-1">
                            <img src="assets/images/resources/welcome.jpg" alt="">
                        </div>
                        <div class="welcome-one__img-2">
                            <img src="assets/images/resources/Welcome-2.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="welcome-one__right">
                    <h2 class="welcome-one__title">Nous co-construisons avec vos équipes</h2>
                    <p class="welcome-one__text" style="color: red;">Nous sommes une équipe de talentueux pionniers dans la Finance Digitale et les Services Numériques qui met
                        ensemble sa vision du marché unique africain et ses connaissances collectives pour essayer de transformer
                        l'industrie du numérique en Afrique en offrant aux Populations et aux Entreprises des produits de finance digitale
                        et des solutions technologiques et d’intégration innovants.</p>
                    <!-- <a class="download" href="download.php?file=assets/Documents/test.pdf">
                                <img src="assets/images/down-b.svg">
                                Télécharger notre présentation</a>-->
                    <!-- 
                            <div class="welcome-one__progress">
                                <div class="welcome-one__progress-single">
                                    <div class="bar">
                                        <div class="bar-inner count-bar" data-percent="88%">
                                            <div class="count-text">88%</div>
                                        </div>
                                    </div>
                                    <h4 class="welcome-one__progress-title">Consulting</h4>
                                </div>
                                <div class="welcome-one__progress-single">
                                    <div class="bar">
                                        <div class="bar-inner count-bar" data-percent="68%">
                                            <div class="count-text">68%</div>
                                        </div>
                                    </div>
                                    <h4 class="welcome-one__progress-title">Advices</h4>
                                </div>
                            </div> -->
                    <div class="welcome-one__big-text">Akili</div>
                </div>
            </div>
        </div>
    </div>
</section>