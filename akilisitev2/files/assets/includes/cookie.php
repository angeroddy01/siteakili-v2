<div class="cookie">
    <img src="/assets/images/cookie.png" alt="">
    <div class="content">
        <header>Consentement aux cookies</header>
        <p> <b>Akili</b> utilise vos cookies pour vous garantir la meilleure expérience possible.</p>
        <div class="buttons">
            <button class="item">J'ai compris.</button>
            <a href="#" class="cok">En savoir plus</a>
        </div>
    </div>
</div>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap');

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
    }

    .cookie {
        z-index: 2;
        position: relative;
        bottom: 0px;
        left: 30px;
        max-width: 300px;
        background: #fff;
        padding: 20px 20px 25px 20px;
        border-radius: 15px;
        box-shadow: 1px 7px 14px -5px rgba(0, 0, 0, 0.15);
        text-align: center;
    }

    .cookie.hide {
        opacity: 0;
        pointer-events: none;
        transform: scale(0.8);
        transition: all 0.3s ease;
    }

    ::selection {
        color: #fff;
        background: #FCBA7F;
    }

    .cookie img {
        max-width: 50px;
    }

    .content header {
        font-size: 12px;
        font-weight: 600;
    }

    .content {
        margin-top: 10px;
    }

    .content p {
        color: #858585;
        margin: 5px 0 20px 0;
        font-size: 12px;
    }

    .content .buttons {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .buttons button {
        padding: 5px;
        border: none;
        outline: none;
        color: #fff;
        font-size: 12px;
        font-weight: 500;
        border-radius: 5px;
        background: #2F358B;
        cursor: pointer;
        transition: all 0.3s ease;
    }

    .buttons button:hover {
        transform: scale(0.97);
    }



    .buttons a {
        color: #2F358B;
        padding: 5px;
        font-size: 12px;
    }
</style>

<script>
    const cookieBox = document.querySelector(".cookie"),
        acceptBtn = cookieBox.querySelector("button");

    acceptBtn.onclick = () => {
        //réglage du cookie pour 1 mois, après un mois il sera expiré automatiquement
        document.cookie = "CookieBy=ElliotTah; max-age=" + 60 * 60 * 24 * 30;
        if (document.cookie) { //si le cookie est défini
            cookieBox.classList.add("hide"); //cacher la boîte à cookies
        } else { //si le cookie n'est pas défini, une erreur est signalée
            alert("Le cookie ne peut pas être défini ! Veuillez débloquer ce site dans la configuration des cookies de votre navigateur.");
        }
    }
    let checkCookie = document.cookie.indexOf("CookieBy=ElliotTah"); //vérification de notre cookie
    //Si le cookie est installé, alors on cache la case cookie, sinon on l'affiche
    checkCookie != -1 ? cookieBox.classList.add("hide") : cookieBox.classList.remove("hide");
</script>