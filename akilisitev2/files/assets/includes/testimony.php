<!--Brand One End-->
<section class="testimonials-one">
    <div class="container">
        <br> <br>
        <div class="row">
            <div class="col-xl-4">
                <div class="testimonials-one__left">
                    <div class="section-title text-left">
                        <h2 class="section-title__title">Ce qu'ils pensent de nous</h2>
                        <span class="section-title__tagline">Ils sont nombreux à nous avoir fait confiance</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="testimonials-one__right">
                    <div class="testimonials-one__carousel owl-theme owl-carousel">
                        <!--Testimonials One Single-->
                        <div class="testimonials-one__single">
                            <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                            <div class="testimonials-one__client-info">
                                <h5 class="testimonials-one__client-name"> MTN </h5>
                                <p class="testimonials-one__client-title">Client</p>
                            </div>
                            <div class="testimonials-one__client-img">
                                <img src="assets/images/testimonial/testim4.jpg" alt="">
                            </div>
                            <div class="testimonials-one__quote"></div>
                        </div>
                        <!--Testimonials One Single-->
                        <div class="testimonials-one__single">
                            <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                            <div class="testimonials-one__client-info">
                                <h5 class="testimonials-one__client-name"> Orange Côte D'ivoire </h5>
                                <p class="testimonials-one__client-title">Client</p>
                            </div>
                            <div class="testimonials-one__client-img">
                                <img src="assets/images/testimonial/testim1.jpg" alt="">
                            </div>
                            <div class="testimonials-one__quote"></div>
                        </div>
                        <!--Testimonials One Single-->
                        <div class="testimonials-one__single">
                            <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                            <div class="testimonials-one__client-info">
                                <h5 class="testimonials-one__client-name">NSIA BANQUE</h5>
                                <p class="testimonials-one__client-title">Client</p>
                            </div>
                            <div class="testimonials-one__client-img">
                                <img src="assets/images/testimonial/testim2.jpg" alt="">
                            </div>
                            <div class="testimonials-one__quote"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>