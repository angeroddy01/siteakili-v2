<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Akili</title>
    <!-- favicons Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/AKILI_Logo Officiel_Plan de travail 1.jpg" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/AKILI_Logo Officiel_Plan de travail 1.jpg" />
    <link rel="manifest" href="assets/images/favicons/site.webmanifest" />

<script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css?version=2" />
    <link rel="stylesheet" href="assets/vendors/animate/animate.min.css" />
    <link rel="stylesheet" href="assets/vendors/fontawesome/css/all.min.css" />
    <link rel="stylesheet" href="assets/vendors/jarallax/jarallax.css" />
    <link rel="stylesheet" href="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.css" />
    <link rel="stylesheet" href="assets/vendors/nouislider/nouislider.min.css" />
    <link rel="stylesheet" href="assets/vendors/nouislider/nouislider.pips.css" />
    <link rel="stylesheet" href="assets/vendors/odometer/odometer.min.css" />
    <link rel="stylesheet" href="assets/vendors/swiper/swiper.min.css?version=1" />
    <link rel="stylesheet" href="assets/vendors/aivons-icons/style.css?version=1">
    <link rel="stylesheet" href="assets/vendors/tiny-slider/tiny-slider.min.css" />
    <link rel="stylesheet" href="assets/vendors/reey-font/stylesheet.css" />
    <link rel="stylesheet" href="assets/vendors/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="assets/vendors/owl-carousel/owl.theme.default.min.css" />
    <link rel="stylesheet" href="assets/vendors/twentytwenty/twentytwenty.css" />
    <link rel="stylesheet" href="assets/vendors/bxslider/css/jquery.bxslider.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <!-- template styles -->
    <link rel="stylesheet" href="assets/css/aivons.css?version=2" />
    <link rel="stylesheet" href="assets/css/aivons-responsive.css" />

    <!-- RTL Styles -->
    <link rel="stylesheet" href="assets/css/aivons-rtl.css">

    <!-- color css -->
    <link rel="stylesheet" id="jssDefault" href="assets/css/colors/color-default.css">
    <link rel="stylesheet" id="jssMode" href="assets/css/modes/aivons-normal.css">

    <!-- toolbar css -->
    <link rel="stylesheet" href="assets/css/aivons-toolbar.css">
<link rel="stylesheet" type="text/css" href="assets/styles.css">
<link rel="stylesheet" href="assets/owl.caroussel.min.css">

    <!-- cookies css -->
    <link rel="stylesheet" href="assets/css/cookies.css">
    <!-- autre css -->
    <link rel="stylesheet" href="assets/css/autre.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>