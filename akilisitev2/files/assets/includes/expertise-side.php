<div class="services-details__sidebar">
    <div class="services-details__services-list-box">
        <h3 class="services-detials__categories">Expertise</h3>
        <ul class="services-details__services-list list-unstyled">
            <li><a href="techAdvice.php">Conseil en technologies <span class="icon-right-arrow"></span></a></li>
            <li><a href="transformation.php">Assistance technique <span class="icon-right-arrow"></span></a></li>
            <li><a href="conduite.php"> Conduite du changement <span class="icon-right-arrow"></span></a></li>
            <li><a href="automatisation.php"> Robotic Process Automation (RPA) <span class="icon-right-arrow"></span></a></li>
            <li><a href="Data.php">Data & Analytics <span class="icon-right-arrow"></span></a></li>
            <li><a href="metier.php"> Développement des solutions <span class="icon-right-arrow"></span></a></li>
        </ul>
    </div>
</div>