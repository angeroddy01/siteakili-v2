<footer class="site-footer">
    <div class="site-footer-shape wow slideInRight" data-wow-delay="100ms" data-wow-duration="3500ms"></div>
    <div class="container">
        <div class="site-footer-bottom">
            <div class="row">
                <div class="col-xl-12 col-md-6 col-sm-6 abonn">
                    <div class="site-footer-bottom__inner">
                        <div class="site-footer-bottom__left">


                            <button class="abonnement">
                                <span class="circle" aria-hidden="true">
                                    <span class="icon arrow"></span>
                                </span>
                                <a href="newsletter1.php" class="button-text"> S’abonner à notre newsletter </a>
                            </button>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-footer-bottom">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="site-footer-bottom__inner">
                        <div class="site-footer-bottom__left">
                            <a href="index.php"><img src="assets/images/Logo AKILI[Miniature Blanc].png" alt=""></a>
                            <a href="#" class="fs-6 p-2">Expertise </a>
                            <!-- <a href="#" class="fs-6 p-2">Notre actualité</a> -->
                            <a href="#" class="fs-6 p-2">Nous rejoindre</a>
                            <a href="#" class="fs-6 p-2">A Propos</a>
                            <a href="#" class="fs-6 p-2">Nous contacter</a>
                        </div>
                        
                        <div class="site-footer__social icon">
                            <a href="#" class="clr-linkedin"><i class="fab fa-linkedin"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                            <a href="#" class="clr-insta"><i class="fab fa-instagram"></i></a>
                            <a href="#" class="clr-dri"><i class="fab fa-youtube"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-footer-bottom">
            <div class="row">
                <div class="col-xl-12">
                    <div class="site-footer-bottom__inner">
                        <div class="site-footer-bottom__left">
                            <a href="#" class="fs-6 p-2 ">Mentions Légales</a>
                            <a href="#" class="fs-6 p-2 ">Conditions d'Utilisation</a>
                            <a href="#" class="fs-6 p-2 ">Cookie Policy</a>
                            <a href="#" class="fs-6  p-2">Accessibilité</a>
                            <a href="#" class="fs-6  p-2">Plan du Site</a>
                        </div>

                        <div class="site-footer__social ">
                            <a class="p-2 fs-6">Cocody 06 BP 2440 Abidjan 06 Côte d’Ivoire  </a>
                            <a class="p-2 fs-6">&copy; <?php echo  date("Y") ?> Akili . Tout droits réservés.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<style>
    
    .fs-6 {
        font-size: 12px !important;
    }

    .icon a i {
        width: 30px;
    }


    button {
        position: relative;
        display: inline-block;
        cursor: pointer;
        outline: none;
        border: 0;
        vertical-align: middle;
        text-decoration: none;
        background: transparent;
        padding: 0;
        font-size: inherit;
        font-family: inherit;
    }

    button.abonnement {
        width: 35rem;
        height: auto;
    }

    button.abonnement .circle {
        transition: all 0.45s cubic-bezier(0.65, 0, 0.076, 1);
        position: relative;
        display: block;
        margin: 0;
        width: 3rem;
        height: 3rem;
        background: #282936;
        border-radius: 1.625rem;
    }

    button.abonnement .circle .icon {
        transition: all 0.45s cubic-bezier(0.65, 0, 0.076, 1);
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto;
        background: #fff;
    }

    button.abonnement .circle .icon.arrow {
        transition: all 0.45s cubic-bezier(0.65, 0, 0.076, 1);
        left: 0.625rem;
        width: 1.125rem;
        height: 0.125rem;
        background: none;
    }

    button.abonnement .circle .icon.arrow::before {
        position: absolute;
        content: "";
        top: -0.25rem;
        right: 0.0625rem;
        width: 0.625rem;
        height: 0.625rem;
        border-top: 0.125rem solid #fff;
        border-right: 0.125rem solid #fff;
        transform: rotate(45deg);
    }

    button.abonnement .button-text {
        transition: all 0.45s cubic-bezier(0.65, 0, 0.076, 1);
        position: absolute;
        top: 0;
        left: 0;
        right: 0;

        bottom: 0;
        padding: 0.75rem 0;
        margin: 0 0 0 1.85rem;
        color: #282936;
        font-weight: 700;
        line-height: 1.6;
        text-align: center;

    }

    button:hover .circle {
        width: 100%;
    }



    button:hover .circle .icon.arrow {
        background: #fff;
        transform: translate(1rem, 0);
    }

    button:hover .button-text {
        color: #fff;
        letter-spacing: 5px;
    }

    button .button-text {
        color: #fff;
        letter-spacing: 5px;
    }

    .site-footer-shape {
        background-image: url(assets/images/shapes/footer-shape.png);
    }

    @media screen and (min-width: 200px) and (max-width: 640px) {

        .abonn {
            width: 100% !important;
            margin: 0 auto !important;

        }

        button:hover .button-text {
            color: #fff;
            letter-spacing: 0px;
        }

        button .button-text {
            color: #fff;
            letter-spacing: 0px;
        }
    }
</style>