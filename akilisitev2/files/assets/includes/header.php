<header class="main-header clearfix">
    <nav class="main-menu clearfix">
        <div class="main-menu-wrapper">
            <div class="main-menu-wrapper__left">
                <div class="main-menu-wrapper__logo">
                    <a href="index.php"><img src="assets/images/Logo AKILI[Miniature Blanc].png" alt=""></a>
                </div>
                <div class="main-menu-wrapper__main-menu">
                    <a href="#" class="mobile-nav__toggler">
                        <span class="mobile-nav__toggler-bar"></span>
                        <span class="mobile-nav__toggler-bar"></span>
                        <span class="mobile-nav__toggler-bar"></span>
                    </a>
                    <ul class="main-menu__list">
                        <li>
                            <a href="index.php">Accueil</a>
                        </li>
                        <li class="dropdown">
                            <a href="#">Expertise</a>
                          <ul>
                                <li>
                                    <a href="#" class="text"> Conseil & Stratégie </a><!-- techAdvice.php -->

                                    <a href="#" class="text"> Assistance technique & AMOA</a><!-- transformation.php -->
                                    <a href="#" class="text"> Conduite du changement </a><!-- conduite.php -->
                                    <a href="#" class="text"> Data & IA </a><!-- iadata.php -->
                                </li>
                                <li>
                                    <a href="#" class="text">Business Process Management (BPM)</a><!-- business.php -->
                                    <a href="#" class="text">Robotic Process Automation (RPA) </a><!-- automatisation.php -->
                                    <a href="#" class="text"> Développement d'applications </a><!-- metier.php -->
                                    <a href="#" class="text"> Paiement digital & Transfert rapide  </a><!-- paiementdigital.php -->
                                </li>
                            </ul> 
                        </li>
                        
                        <li>
                            <a href="index3.php">Nous rejoindre</a><!-- index3.php -->
                        </li>
                        <li>
                            <a href="about.php">A propos</a>
                        </li>
                        <li>
                            <a href="contacts.php">Nous contacter</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-menu-wrapper__right">
                <div class="main-menu-wrapper__social-box">
                    <div class="main-menu-wrapper__social">
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                        <a href="#" class="clr-dri"><i class="fab fa-dribbble"></i></a>
                        <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>