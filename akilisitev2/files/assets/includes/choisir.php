 <section class="reasons">
     <div class="container">
         <div class="reasons-bg"></div>
         <div class="row">
             <div class="col-xl-5 col-lg-6">
                 <div class="reasons__left">
                     <h2 class="reasons__title">Pourquoi nous choisir ?</h2>
                     <ul class="list-unstyled reasons__list-box">
                         <li>
                             <div class="reasons__icon">
                                 <span class="icon-tick"></span>
                             </div>
                             <div class="reasons__content">
                                 <h4 class="reasons__content-title">Une équipe professionnelle</h4>
                                 <p class="reasons__content-text">Akili, c'est une équipe jeune, dynamique et surtout professionnelle</p>
                             </div>
                         </li>
                         <li>
                             <div class="reasons__icon">
                                 <span class="icon-tick"></span>
                             </div>
                             <div class="reasons__content">
                                 <h4 class="reasons__content-title"> Une expérience dans le domaine </h4>
                                 <p class="reasons__content-text"> A travers nos projets pour nos illustres partenaires, nous avons acquis une expérience précieuse dans notre domaine</p>
                             </div>
                         </li>
                         <li>
                             <div class="reasons__icon">
                                 <span class="icon-tick"></span>
                             </div>
                             <div class="reasons__content">
                                 <h4 class="reasons__content-title"> Une entreprise à l'écoute </h4>
                                 <p class="reasons__content-text">Travailler avec Akili, c'est travailler avec une entreprise à l'écoute, et toujours disponible</p>
                             </div>
                         </li>
                     </ul>
                 </div>
             </div>
             <div class="col-xl-7 col-lg-6">
                 <div class="reasons__img-box">
                     <div class="reasons__img">
                         <img src="assets/images/resources/reason.jpg" alt="">
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>