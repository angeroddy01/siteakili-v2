<!DOCTYPE html>
<html lang="fr">

<!-- Head debut -->
<?php include("assets/includes/head.php"); ?>
<!--- Head Fin -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- header start --->
        <?php include("assets/includes/header.php")   ?>
        <!-- header end -->
        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <section class="main-slider">
            <!-- Swiper -->
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="" id="myVideo">
                                <source src="assets/images/video test_compressed.mp4" type="video/webm">
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--  Texte principal -->
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms" data-wow-duration="4000ms">
                                        <h3 class="text-center main-text">Accompagner.</h3>
                                        <h4 class="text-center mt-3">Votre transformation</h4>
                                        <!--
                                        <div class="center-con">
                                            <div class="round ">
                                                <div id="cta">
                                                    <span class="arrow primera next "></span>
                                                    <span class="arrow segunda next "></span>

                                                </div>
                                            </div>
                                            <a href="about.php">
                                                <h5>En savoir plus</h5>
                                            </a>
                                        </div>-->
                                    </div>
                                    <!--  Fin texte principal -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="" id="myVideo">
                                <source src="assets/images/green_compressed.mp4" type="video/webm">
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <h3 class="text-center main-text">Dans une démarche</h3>
                                        <h4 class="text-center mt-3">de co-construction</h4>
                                        <!--
                                        <div class="center-con">
                                            <div class="round ">
                                                <div id="cta">
                                                    <span class="arrow primera next "></span>
                                                    <span class="arrow segunda next "></span>
                                                </div>
                                            </div>
                                            <a href="about.php">
                                                <h5>En savoir plus</h5>
                                            </a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="" id="myVideo">
                                <source src="assets/images/video-test-2_compressed.mp4" type="video/webm">
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <h3 class="text-center main-text">Passez à la</h3>
                                        <h4 class="text-center mt-3">vitesse supérieure avec nos solutions Data & Analytics</h4>
                                        <!--
                                        <div class="center-con">
                                            <div class="round ">
                                                <div id="cta">
                                                    <span class="arrow primera next "></span>
                                                    <span class="arrow segunda next "></span>
                                                </div>
                                            </div>
                                            <a href="about.php">
                                                <h5>En savoir plus</h5>
                                            </a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <!--Welcome One Start-->
        <section class="welcome-one">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="welcome-one__left">
                            <div class="welcome-one__img-box">
                                <div class="welcome-one__img-1">
                                    <img src="assets/images/resources/welcome.jpg" alt="">
                                </div>
                                <div class="welcome-one__img-2">
                                    <img src="assets/images/resources/Welcome-2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="welcome-one__right">
                            <h2 class="welcome-one__title">Nous co-construisons avec vos équipes</h2>
                            <p class="welcome-one__text" style="color: red;">Nous sommes une équipe de talentueux pionniers dans la Finance Digitale et les Services Numériques qui met
                                ensemble sa vision du marché unique africain et ses connaissances collectives pour essayer de transformer
                                l'industrie du numérique en Afrique en offrant aux Populations et aux Entreprises des produits de finance digitale
                                et des solutions technologiques et d’intégration innovants.</p>
                            <a class="download" href="download.php?file=assets/Documents/test.pdf">
                                <img src="assets/images/down-b.svg">
                                Télécharger notre présentation</a>
                            <!-- 
                            <div class="welcome-one__progress">
                                <div class="welcome-one__progress-single">
                                    <div class="bar">
                                        <div class="bar-inner count-bar" data-percent="88%">
                                            <div class="count-text">88%</div>
                                        </div>
                                    </div>
                                    <h4 class="welcome-one__progress-title">Consulting</h4>
                                </div>
                                <div class="welcome-one__progress-single">
                                    <div class="bar">
                                        <div class="bar-inner count-bar" data-percent="68%">
                                            <div class="count-text">68%</div>
                                        </div>
                                    </div>
                                    <h4 class="welcome-one__progress-title">Advices</h4>
                                </div>
                            </div> -->
                            <div class="welcome-one__big-text">Akili</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--DG Start-->
        <!--Welcome One End-->
        <!--Real World Start-->
        <section class="real-world">
            <div class="real-world-shape wow slideInLeft" data-wow-delay="100ms" data-wow-duration="2500ms" style="background-image: url(assets/images/shapes/real-world-shape.png)"></div>
            <div class="container-fluid">
                <div class="section-title text-center">
                    <h2 class="section-title__title">Notre Expertise métier </h2>
                    <span class="section-title__tagline" style="color: red;"> Profitez de notre expertise et de notre professionnalisme</span>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!--Real World Single-->
                        <div class="real-world__single">
                            <h2 class="real-world__title"><a href="conseil.php">Conseil & <br>stratégies</a>
                            </h2>
                            <a href="conseil.php" class="real-world__btn">En savoir plus</a>
                            <div class="real-world__icon-box">
                                <span class="icon-report"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <!--Real World Single-->
                        <div class="real-world__single">
                            <h2 class="real-world__title"><a href="assistance.php">Assistance <br>technique </a></h2>
                            <a href="assistance.php" class="real-world__btn">En savoir plus</a>
                            <div class="real-world__icon-box">
                                <span class="icon-data-analytics"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <!--Real World Single-->
                        <div class="real-world__single">
                            <h2 class="real-world__title"><a href="solutions.php">Implémentation<br>des solutions</a>
                            </h2>
                            <a href="solutions.php" class="real-world__btn">En savoir plus</a>
                            <div class="real-world__icon-box">
                                <span class="icon-software"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <!--Real World Single-->
                        <div class="real-world__single">
                            <h2 class="real-world__title"><a href="solutions.php">Conduite<br>du changement</a></h2>
                            <a href="solutions.php" class="real-world__btn">En savoir plus</a>
                            <div class="real-world__icon-box">
                                <span class="icon-software"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Real World Start-->


        <section class="container mt-5">
            <div class="row cases-row">
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas">
                    <div class="panel wow fadeInUp" data-wow-delay="0ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAMgCAYAAAD/YBzEAAAgAElEQVR4Xu3debCd510f8Fe62lfLkmxtliVr85q1pFAoKQUChdC0UMp0yQTakkLChACl0wChaZpkpsNSUkoaSltC02kp0EJbmoZCSKANhCWJE1uyrcWSLGuXte9rn+co5/jec7dzpa9PrXs/7z+Zsd7zPed83l/mfud93/O80175ZT9xo7ERIECAAAECBAjEBKYpWDFLQQQIECBAgACBloCCZRAIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqbmoKXLt2qTl/9rnm0qWjzbWrF5pp06Y3AwNzm9lzljVz561uBmbMvS2YK5dPNefP7Wvq/16/frmZOXNxM2vO0mbe/LXlvabdVnb7xTduXG/Ond3bXL70QnP1yplm+sDs1vvMW7C2/O/CyHtMhpDr1680F84faC5dONxcvXahuXH9auv4tqzm39fMnLUo8jX7dTz6MVsRECEE7jABBesOO2A+7stLoJaqI4c+1Zw+ua25cePaKB9uWqtkLb77seaeFX+hVb563c6e2d0cPvDbzdnTO8tLbgx72cxZdzVLl//Zkvs1zbTpA73GDtmvFobDB36nOX7sT0uxOj08o3zehYs2NytWf0MpEGtu6T0mw4suXjjUHDn4qebk8S+UY3111K9Uj/Wye7+qWbL0tbdUfvt1PPoxW5PhuPsOBG5VQMG6VTmvm9IC9Y/ggef+R/PC0c+MWHxGw3nstR9opk+f2ZPdof3/uxSfT5R9r4+7/9x5a5p1G9/czJp997j7Dt7hYjkLs2fnR5tLFw+P+7pp0waalWu+qVm+4qvH3Xey7XDk0O81h57/X2OU6OHfeMHCDc39G/52M2Pmgp45+nU8+jFbPX9pOxKYpAIK1iQ9sL7WSydw7drFZveOX2zOnXl2xDepBWratBlN3a/7rFOvBasWq0P7Pz4kf1rJnTPnnnLpbk4pREeHnW2aXf5t00Pf1/PlyCvlMuCObf+iXHY8OeR96lmx2bOXti5/XbpwZNjZmjXr/lrrrNlU2Q4f/N1WuereZsxY2Myeu7x1rOuZv1qOuo93PZu14cHvKZeL54zL1a/j0Y/ZGvfL2oHAFBBQsKbAQfYVcwL1vpjdO/5dc+bUM0NC77r7VeWS0Gua+QvXd/6Y3rh+rblYzgydK5f5Tp14sjl7Zlfz2GvfP+4ZrDOndjTPbv+FIX+s6yWne1d9XTNjxvzW+964caN8hqebfXt+bUjRWnzXo826TW/p6QvvfOpD5Z6r3Z19Z81a0qxZ9+3NwsWbOv+t/tE/tP+3muNH/6jz3+qZrI0Pvb11v9Fk3+plwWe2/vMC/uJZxDlzVzWr739Ts2DhA0O+/pUrZ8slxN9tjh3+v0OO3fIVr29W3ffGcan6cTz6NVvjflk7EJgCAgrWFDjIvmJO4NiRP2j27/31TmC9kX3dxrc0CxZtGPdN6lmnWeXM0Fj3YNXitGPbB8tN1Ps7efeu+vpy/9MbRsy/fOl4s33rB8vZsvOdf9/44NtL0Vs35uc5dWJruTT4kc4+M2YuajY//I5yg/biEV9XL4cePfz7nX9bUO7J2rDlu8f9znf6Dvt2/0q5N+1POl+jlqtaLgcGZo361Y6WgnXguf82qJDOaB559XvKa2aP+pp+HI9+zdadfsx9fgIpAQUrJSln0gvUX1s9/eRPNtdbl/6a1qWhjQ+9LXomp/sP7Zx5q0rx+f4xS1ktALUItLcFizaV8vPWMY9HPStzsfwSrr3dv+HNzV13v2LU11wvv5TbvvWnW5cm29vGB9/WOmM3mbetj7+39YvK9vbA5rcOOcM30nevRWZ79b1wsPPP6zZ+Z7N4ySOjUvXjePRrtibzPPhuBCYioGBNRMu+U1qg+yzOWGeWbhVqz67/0Jwqv1Jrb2vX/41mybLXjBlXL1tu+8L7B10qnNY8/Kp3j7q0Qr1X6JlSFNvbrNnLmgcf+4fj/uKt++zd0uVfUS4pfuutftWX/etqqXzis+/qfM7p02c3j77mvT39CvTg8x8vlwvrDxRubqvu+5ZRfxzQr+PRj9l62R9UH5BAHwUUrD5ie6s7V6DeT7XtC/+0uXr1XOtL1D+2D7/qx8e8VDTRb1vf48nH39M5QzZ9+qzWpaVefnV4YN9vNkfLL93a21g3oh85+Mnm4PMf6+xbl1+o93eNt9X1verna9+PVC8nPvzKHxvvZXfsv1+9er7Z+vl/3Pn8s+csbxXRXrYXjnymeX7vf+nsWn2r80hbP45Hv2arFxv7EJgqAgrWVDnSvudtCdSb1Pfs/KVOxt3LXtfct/7bbyuz+8X1ZvidT3+o85/rz/zrL9B62eo6XPWXje1t8d2vbNaVJQJG2nY986/Lulo7Ov+04cHvHXbD9mjvWe/3unD++c4/b3n0HzRz5t7by0ccc596I31di6u9LVy0pVm/+e+Oe1at7t9dZuov9zaWX1NOnz7jtj5XPTP4xGd/tPMryvrryodf+aM9ZR49/H/KfVj/vbPvqvv+cjmD9ef/vx2Pfs1WTzh2IjBFBBSsKXKgfc3bE9hf/lgeK38029t49yzdyrt1F4V7Vv7Fsu7UX+opqp5Z2/r593T2raWnlp+Rtq2PlzNxnQVFp5dfNr6vp7NkNWv/3t9ojh35dNyhlpldz/z8kKUvVpTvfm8xGGu7cP5gs+Opny0n1a60dqtnFjc/8v1lBf3lPbmNt9POp//VoM80rXmknLXsZV2r55795ebEC5/txG8o96stGOV+tX4cj37N1nie/p3AVBJQsKbS0fZdb1lg51M/V5Y02NN5fb00Vi+R1bWuTr7weHPi+OPlETPHSnE523pETv23egZqcblxfP6C+3t63+57vOqSCUuXv66n19adnvjcu4fcgF+XhOj+xWL9vE+W/drbRC/zHT1Uzszse/HMzIrV31guL35tz59xrB3rjwjqzeHty7Blafpm45bvHfUXkdevXW62l19cXrp4pBO79oFyz1pZLiO1nXjh881zz/7HTlwv9921fgzxxD8rjzS6WfrqL0dv3uM2fAX/fh2PfsxWylwOgckioGBNliPpe7xkAvVXYU9+7sdazwCsWy1Q9Wbn02UtrH27//OQX5mN9CEWLn6w3Az+bc2scolprG33jo+UR+5s7ezywOa/V36xtqXn7/XMkz9Vfrl2qLP/Q6/4kfLHfcmQ19flH7Zv/ZnOf5tXyl9dnLTX7eTxLzZ7d320s/uSZX+mWbv+O3p9+bj7nT75dGudsfaCnfWy3OZHfqCs/zVv2GufK7+cPDFoCYW7l31ZuWz718d9j4nsUI/97u3/tjlz+kvrnpWSdP8Df2vUX1xeKWcGd2//xSGXUcf6BWG/jkc/ZmsirvYlMBUEFKypcJR9x9sS6D7LUC+/3bPya8uZjf/UKQLjvUFdZ6oWprnzVo66686nP1wuR+3q/PvmR97ZeoZhr9uu8vq6mGl72/zIDw57v7Ond5VLcR/u7LPorkea9Zu+s9e3KPnPNrvKZbP2NpGFTXt9kwP7/me5Yf9TY37GEy987kv+N3erq9jX5Symj7E+Va/v371ffZD33vLrzrqwa3urxXfxkle0LkXWe73qgqznim1dMuNaWQH/5jatXOL95jIrrx/1rft1PPoxW7fq63UEJquAgjVZj6zvFRO4fOlk89QX39/JmzlzcesyVvuBv/WPbD17UstQfZzNlUsnmlMnn2yt3j740Sn1OYGbH37nqI+yGX4D+Q+XG8jv6fl7PFvPtAwqASMtOHr65FNfOkN0M/aucjN8fV5er9v5c/taj9dpby/FgqP1odn13qfzZ/d23mfV2nKT+L03bxKva3FVq+vXL92sMcV800PvKP4rev0aE96vnsk6deKLrVXaz7U+1/AHbw8OrbOw8r5vLg/JfnFV/JHetF/Hox+zNWFULyAwyQUUrEl+gH292xe4cP5Q+YP+UyMGLV3+5c2qtW8a8RdrZ05tb/3ysH1psQbcXZ7hd195lt9I29NP/MSQ+4keesW7JvTw5j07/30pAU90oh/Y/N3lEuPmIW91sqyxVc/GtLclS8slvgd6v8RXbyqvC462t3kL1pVy8/bbR+5KuFxKar0fq3026Objeb6vFM4Vzc6n/uWQle7X3P9tzdJ7vjz+GQYH1pvwq93xo3/cOos31gO46yOE6r1a9dLwtGnTxvxc/Toe/Zitl/QACCdwBwooWHfgQfOR+yvQXSra717/gD5QlhIYa+u+Z6mu/v7QK99VFgFdNOxlw/8IDr+Haqz3Gl6wht/DNewP+gTvoeoumxO9h2siR657aYx6s3j94cDxY3/ciRlrOYqJvNdY+9bHEdVSWs/eTWSbM3dlq7yOdZm3X8ejH7M1ERv7EpgKAgrWVDjKvuNtCVy6eKz1q7DubcujvV3C636I7+r7/2qz7J4/NyyvH5dx+nVJ6rbAB724e1mIwbm1cNX71AYG5qTebljO5csnyxmzn2uulP9tb/WB2/Xh24sWP9TMmlOfLTmjXDI+W7tFuM0AAB0TSURBVC5pPte8cPQPyxpjOzv71suX6zf9nXKpcOOIn7Ffx6Mfs/WSHQTBBO5QAQXrDj1wPnb/BOoNzNvKM+kGb3PnrWmtt9TL9sLRP2qe3/NrnV1Hu+9p6JpLTas8TMWb3Aeb1sfV1IIzeHHT+u83Lxm+PfocyJGO5bPP/EL5BeH2zj/Nm7+2Wbfpu8oZyAWjHvpjhz/d7H/uNzr/PjAwr9ny2A+NeNayfze5D17P66WZrV7+v2AfAlNJQMGaSkfbd70lgbqe0ROf/ZEhr11azkCtKWeietm6LzHOmbuqLAL6A8NeWldiryuyt7eR7qEa6/16WqbhXFmmYdvgZRomdg/VS71Mw0jfry6HsXv7vxnyT2M9eqaXY9LLPufKGamdZRHT9laX59hS1rMaq1y19+0+87bsnq9qVt//pmFve6FPx6Mfs9WLqX0ITCUBBWsqHW3f9ZYFtn3hfeUy0anO6yeywObVK2WV9foMvy9toz1ypXu1+NtbaHSgrND+gT4sNNrbcwxvFb6ewdqx7WfL+l4HhkS8lMsytN/o4L6PNUcOfbLzvstXvL48tPmNPX2VK5dPl2dXvq/se/PXhgPlsuKj5bmS3Vt+odGRj0c/ZqsnGDsRmEICCtYUOti+6q0L7CqXis4OulS0cs0bx1zfaPA7df8RnTFjQXmI84sPEW7ve3uPMxn6YOI77VE5ox2Z5/f+ennW4B+M+M9LytIYa8MLiw5+o+5lL+q9VIvueqjnIer1xvKX/6Nyep+tnnHsSGAKCChYU+Ag+4q3L9B9BqAuNLpyzTf2FNy9jlY9+/LgYz887LXDH8i7sTzs+e/39B7dN0vXRTDXbXzziK99OT7seaQPerIsObG3LD3R3mbNXtY6Izf00Th/szwa59U9GU10p+4fJ2x6+B0Tuuer+566TWUh1Hnz1wz7GP04Hv2arYka25/AZBZQsCbz0fXdYgLd9wFNZAXz02Xxz/q4lfa2oPyibMOW4cXpxvVrzZPlUuL18rzAuk2fPquc6XpPTw9i7l79fE1Za2tpWXNrpO3IwU82B5//WOefVqzu7TLftasXyqXOf1IWWL3Weu1En2M4kYMx2jpY9eb2utBpe5HXmw93fmdZUX3ZROJ72rf7DFY9ZvXY9brVRxLVR+G0t/o8wpEeQt2P49Gv2erVxn4EpoKAgjUVjrLveNsC3X+g6i/DHn7Vu0dcYLT7zbrPfo11g/aest7SqbKgZXtb+8D4Z2jqIphPfeED5XEt7XvEppXPVh5GPcJaWzX34oXDzTNP/mTnPeof/frHf7zt2JE/bPbv/a+d3ZYu/4ryjMVvHe9lE/731kruT32orDv1XOe1g1dyP1YuGe4vlw7bW/1FZ/1FYX1kTXLbt/tXh6y5tbLcf3VPuQ+rl63+MOLJz/14pwjW1zz66veOuIp/v45HP2arFxv7EJgqAgrWVDnSvudtC+wrSy0cL0sutLdeVhCvN7jXNbRefD5d01qRfH55yPJI26kTW8vq7x8ZVB5WN/XSVL00Ntp2/Nhny0Onf7nzzwvK41k2bHnrmN/3mbJK+sXzL944vm7jW8qz9R4d9TX1ZvO6svrgy3MbHnxbWfhz/W27dgcMfxbhw2Utqe8aslv3oqrLymN0VpfH6SS348f+tPUw7/Y2e8695defPzjmsWjv211G6wr0Wx79oVE/Xj+OR79mK3kMZBG4kwUUrDv56PnsfRWo91LVstS5PFUWuKyPial/PEfa6pmYPTt+qTl96qnOP88vj5apZ1tG2+oz73Zs++CQS0v3rnpDs2L114/4ktaltLL/tfJsxPa2sRSf+eMUn+4/tvX5irXIzZw1fIX5mntg32+WBzD/3oRK3K0cnJuXYuvl1Ju/vquXIetDq2fMmDckrl6urIXv8uUTnf9eS9iiux6+lbcd8TVXr5xttn3xA82NcjaqvS1f8dXll4TfMuZ7XCjFtd5/1b7UW3ceb1mJfhyPfs1W7AAIInCHCyhYd/gB9PH7K3Dw+Y83Rw5+ovOmA+UPf/2Du+TuV5eHDg90/nt9pMyBstjk2TO7Ov+t3j9Uz/rMX7B2zA9dn2H4bGvdpxcfKFzP0NQ/0oOLxumTT7cWMH3x0mDTTOTesO6buOvDqOvSEINXHb9SSsbh/b9VVij/zJDv8VIs8lmXNqilqa6KfnObXry+Z9SzZPWhyzuf/lBhut7aux6LLaWM1VKW2rrPptXcRXc9UgrvN5RFYFcOeZv6a9H6rMJDxWvw8yfr+lkPvuIfDSuJ3Z+xH8ejX7OV8pdD4E4WULDu5KPns/ddoN7vVB/gPHhB0FYVKGez5pYzWfXRKPWs0uVLx4Z9ttVr/0p5xMpX9vSZDx/4RPlD/fEh+9bsOeUy1cDA7OZSyR+8Llfdsf46cVO5/DgwY25P73HlyunWGlODHwNTXzhz1pJmdnkMTb2sWe8Pap+xa4f2cmm0pw8waKfqWldNP3vmxcfM9LLWWPcN4vMXPtD6AcFYl1Qn8tnqvVS7nvn58hicvcNeVotcfVzP9PKonCulFF68cKhT9jo7l0u76zfWM2sPjvu2/Toe/Zitcb+sHQhMAQEFawocZF8xK3Dt2uVy5uhXm5PHH+8puJ65qr/qu7s8WHkiWz0TUv8YDj6TNdrr643edVmGehZqIlstUHt2frTcW3V43JfV77FyzTc19TJZejt84HdaZ37aW/21Xl3JfryiVC971VXeBz/OZrzLcRP97Fevni/3Yv1KKdVbJ/TS+szC+9Z/x4TWzurX8ejHbE0Iy84EJqGAgjUJD6qv1B+Bk+XXfkcP/f6QX7sNfud6xumush7VvavfUM4ITaz4tHPOnnm2lKzfLouc1kuNL14ybP97XRW+Lsdwz4qvGXKJciIC9SxNLTjHj/1Jc7U8d3H4Nr1ZuHhz67LYSOs4TeS9Rtr37Jndza6nP1z+6ealvroQ6+byKKHRfgXZnVEvY27f+tODPvu01lmsBYs23O5HG/L6ep/UsSOf/tLDnIcfi/bOM8qvN5cuf115oPdXNjPGeGbhaB+uX8ejH7MVPQDCCNxhAgrWHXbAfNyXn8Cli8fKTekHyr1Qp8uNzZfLH9X5rTNJ8xesjy0dcPnyyebCuefLTd0nWzdd1z/i9TLevHI/13hneXoVq5fpzp3dUy5vHm99l4GyxlQtcPU9Zs5c2GvMpN+v3mt1vh6LSy+UHxdcaK0LNlAuEdcyVR/OnVqTq1/Hox+zNemHwhckMIKAgmUsCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFvh/GqnM0YgKk+cAAAAASUVORK5CYII=">
                            </a>
                        </div>
                        <div class="bp-category-tag">Ré-ingénierie des Processus</div>
                        <div class="text-content">
                            <a href="" class="case-title">
                                <h3>Lorem ipsum dolor</h3>
                            </a>
                            <div class="real-content">
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia recusandae, commodi rem magni optio obcaecati
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 50px;">
                    <div class="panel wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAMgCAYAAAD/YBzEAAAgAElEQVR4Xu3debCd510f8Fe62lfLkmxtliVr85q1pFAoKQUChdC0UMp0yQTakkLChACl0wChaZpkpsNSUkoaSltC02kp0EJbmoZCSKANhCWJE1uyrcWSLGuXte9rn+co5/jec7dzpa9PrXs/7z+Zsd7zPed83l/mfud93/O80175ZT9xo7ERIECAAAECBAjEBKYpWDFLQQQIECBAgACBloCCZRAIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqbmoKXLt2qTl/9rnm0qWjzbWrF5pp06Y3AwNzm9lzljVz561uBmbMvS2YK5dPNefP7Wvq/16/frmZOXNxM2vO0mbe/LXlvabdVnb7xTduXG/Ond3bXL70QnP1yplm+sDs1vvMW7C2/O/CyHtMhpDr1680F84faC5dONxcvXahuXH9auv4tqzm39fMnLUo8jX7dTz6MVsRECEE7jABBesOO2A+7stLoJaqI4c+1Zw+ua25cePaKB9uWqtkLb77seaeFX+hVb563c6e2d0cPvDbzdnTO8tLbgx72cxZdzVLl//Zkvs1zbTpA73GDtmvFobDB36nOX7sT0uxOj08o3zehYs2NytWf0MpEGtu6T0mw4suXjjUHDn4qebk8S+UY3111K9Uj/Wye7+qWbL0tbdUfvt1PPoxW5PhuPsOBG5VQMG6VTmvm9IC9Y/ggef+R/PC0c+MWHxGw3nstR9opk+f2ZPdof3/uxSfT5R9r4+7/9x5a5p1G9/czJp997j7Dt7hYjkLs2fnR5tLFw+P+7pp0waalWu+qVm+4qvH3Xey7XDk0O81h57/X2OU6OHfeMHCDc39G/52M2Pmgp45+nU8+jFbPX9pOxKYpAIK1iQ9sL7WSydw7drFZveOX2zOnXl2xDepBWratBlN3a/7rFOvBasWq0P7Pz4kf1rJnTPnnnLpbk4pREeHnW2aXf5t00Pf1/PlyCvlMuCObf+iXHY8OeR96lmx2bOXti5/XbpwZNjZmjXr/lrrrNlU2Q4f/N1WuereZsxY2Myeu7x1rOuZv1qOuo93PZu14cHvKZeL54zL1a/j0Y/ZGvfL2oHAFBBQsKbAQfYVcwL1vpjdO/5dc+bUM0NC77r7VeWS0Gua+QvXd/6Y3rh+rblYzgydK5f5Tp14sjl7Zlfz2GvfP+4ZrDOndjTPbv+FIX+s6yWne1d9XTNjxvzW+964caN8hqebfXt+bUjRWnzXo826TW/p6QvvfOpD5Z6r3Z19Z81a0qxZ9+3NwsWbOv+t/tE/tP+3muNH/6jz3+qZrI0Pvb11v9Fk3+plwWe2/vMC/uJZxDlzVzWr739Ts2DhA0O+/pUrZ8slxN9tjh3+v0OO3fIVr29W3ffGcan6cTz6NVvjflk7EJgCAgrWFDjIvmJO4NiRP2j27/31TmC9kX3dxrc0CxZtGPdN6lmnWeXM0Fj3YNXitGPbB8tN1Ps7efeu+vpy/9MbRsy/fOl4s33rB8vZsvOdf9/44NtL0Vs35uc5dWJruTT4kc4+M2YuajY//I5yg/biEV9XL4cePfz7nX9bUO7J2rDlu8f9znf6Dvt2/0q5N+1POl+jlqtaLgcGZo361Y6WgnXguf82qJDOaB559XvKa2aP+pp+HI9+zdadfsx9fgIpAQUrJSln0gvUX1s9/eRPNtdbl/6a1qWhjQ+9LXomp/sP7Zx5q0rx+f4xS1ktALUItLcFizaV8vPWMY9HPStzsfwSrr3dv+HNzV13v2LU11wvv5TbvvWnW5cm29vGB9/WOmM3mbetj7+39YvK9vbA5rcOOcM30nevRWZ79b1wsPPP6zZ+Z7N4ySOjUvXjePRrtibzPPhuBCYioGBNRMu+U1qg+yzOWGeWbhVqz67/0Jwqv1Jrb2vX/41mybLXjBlXL1tu+8L7B10qnNY8/Kp3j7q0Qr1X6JlSFNvbrNnLmgcf+4fj/uKt++zd0uVfUS4pfuutftWX/etqqXzis+/qfM7p02c3j77mvT39CvTg8x8vlwvrDxRubqvu+5ZRfxzQr+PRj9l62R9UH5BAHwUUrD5ie6s7V6DeT7XtC/+0uXr1XOtL1D+2D7/qx8e8VDTRb1vf48nH39M5QzZ9+qzWpaVefnV4YN9vNkfLL93a21g3oh85+Mnm4PMf6+xbl1+o93eNt9X1verna9+PVC8nPvzKHxvvZXfsv1+9er7Z+vl/3Pn8s+csbxXRXrYXjnymeX7vf+nsWn2r80hbP45Hv2arFxv7EJgqAgrWVDnSvudtCdSb1Pfs/KVOxt3LXtfct/7bbyuz+8X1ZvidT3+o85/rz/zrL9B62eo6XPWXje1t8d2vbNaVJQJG2nY986/Lulo7Ov+04cHvHXbD9mjvWe/3unD++c4/b3n0HzRz5t7by0ccc596I31di6u9LVy0pVm/+e+Oe1at7t9dZuov9zaWX1NOnz7jtj5XPTP4xGd/tPMryvrryodf+aM9ZR49/H/KfVj/vbPvqvv+cjmD9ef/vx2Pfs1WTzh2IjBFBBSsKXKgfc3bE9hf/lgeK38029t49yzdyrt1F4V7Vv7Fsu7UX+opqp5Z2/r593T2raWnlp+Rtq2PlzNxnQVFp5dfNr6vp7NkNWv/3t9ojh35dNyhlpldz/z8kKUvVpTvfm8xGGu7cP5gs+Opny0n1a60dqtnFjc/8v1lBf3lPbmNt9POp//VoM80rXmknLXsZV2r55795ebEC5/txG8o96stGOV+tX4cj37N1nie/p3AVBJQsKbS0fZdb1lg51M/V5Y02NN5fb00Vi+R1bWuTr7weHPi+OPlETPHSnE523pETv23egZqcblxfP6C+3t63+57vOqSCUuXv66n19adnvjcu4fcgF+XhOj+xWL9vE+W/drbRC/zHT1Uzszse/HMzIrV31guL35tz59xrB3rjwjqzeHty7Blafpm45bvHfUXkdevXW62l19cXrp4pBO79oFyz1pZLiO1nXjh881zz/7HTlwv9921fgzxxD8rjzS6WfrqL0dv3uM2fAX/fh2PfsxWylwOgckioGBNliPpe7xkAvVXYU9+7sdazwCsWy1Q9Wbn02UtrH27//OQX5mN9CEWLn6w3Az+bc2scolprG33jo+UR+5s7ezywOa/V36xtqXn7/XMkz9Vfrl2qLP/Q6/4kfLHfcmQ19flH7Zv/ZnOf5tXyl9dnLTX7eTxLzZ7d320s/uSZX+mWbv+O3p9+bj7nT75dGudsfaCnfWy3OZHfqCs/zVv2GufK7+cPDFoCYW7l31ZuWz718d9j4nsUI/97u3/tjlz+kvrnpWSdP8Df2vUX1xeKWcGd2//xSGXUcf6BWG/jkc/ZmsirvYlMBUEFKypcJR9x9sS6D7LUC+/3bPya8uZjf/UKQLjvUFdZ6oWprnzVo66686nP1wuR+3q/PvmR97ZeoZhr9uu8vq6mGl72/zIDw57v7Ond5VLcR/u7LPorkea9Zu+s9e3KPnPNrvKZbP2NpGFTXt9kwP7/me5Yf9TY37GEy987kv+N3erq9jX5Symj7E+Va/v371ffZD33vLrzrqwa3urxXfxkle0LkXWe73qgqznim1dMuNaWQH/5jatXOL95jIrrx/1rft1PPoxW7fq63UEJquAgjVZj6zvFRO4fOlk89QX39/JmzlzcesyVvuBv/WPbD17UstQfZzNlUsnmlMnn2yt3j740Sn1OYGbH37nqI+yGX4D+Q+XG8jv6fl7PFvPtAwqASMtOHr65FNfOkN0M/aucjN8fV5er9v5c/taj9dpby/FgqP1odn13qfzZ/d23mfV2nKT+L03bxKva3FVq+vXL92sMcV800PvKP4rev0aE96vnsk6deKLrVXaz7U+1/AHbw8OrbOw8r5vLg/JfnFV/JHetF/Hox+zNWFULyAwyQUUrEl+gH292xe4cP5Q+YP+UyMGLV3+5c2qtW8a8RdrZ05tb/3ysH1psQbcXZ7hd195lt9I29NP/MSQ+4keesW7JvTw5j07/30pAU90oh/Y/N3lEuPmIW91sqyxVc/GtLclS8slvgd6v8RXbyqvC462t3kL1pVy8/bbR+5KuFxKar0fq3026Objeb6vFM4Vzc6n/uWQle7X3P9tzdJ7vjz+GQYH1pvwq93xo3/cOos31gO46yOE6r1a9dLwtGnTxvxc/Toe/Zitl/QACCdwBwooWHfgQfOR+yvQXSra717/gD5QlhIYa+u+Z6mu/v7QK99VFgFdNOxlw/8IDr+Haqz3Gl6wht/DNewP+gTvoeoumxO9h2siR657aYx6s3j94cDxY3/ciRlrOYqJvNdY+9bHEdVSWs/eTWSbM3dlq7yOdZm3X8ejH7M1ERv7EpgKAgrWVDjKvuNtCVy6eKz1q7DubcujvV3C636I7+r7/2qz7J4/NyyvH5dx+nVJ6rbAB724e1mIwbm1cNX71AYG5qTebljO5csnyxmzn2uulP9tb/WB2/Xh24sWP9TMmlOfLTmjXDI+W7tFuM0AAB0TSURBVC5pPte8cPQPyxpjOzv71suX6zf9nXKpcOOIn7Ffx6Mfs/WSHQTBBO5QAQXrDj1wPnb/BOoNzNvKM+kGb3PnrWmtt9TL9sLRP2qe3/NrnV1Hu+9p6JpLTas8TMWb3Aeb1sfV1IIzeHHT+u83Lxm+PfocyJGO5bPP/EL5BeH2zj/Nm7+2Wbfpu8oZyAWjHvpjhz/d7H/uNzr/PjAwr9ny2A+NeNayfze5D17P66WZrV7+v2AfAlNJQMGaSkfbd70lgbqe0ROf/ZEhr11azkCtKWeietm6LzHOmbuqLAL6A8NeWldiryuyt7eR7qEa6/16WqbhXFmmYdvgZRomdg/VS71Mw0jfry6HsXv7vxnyT2M9eqaXY9LLPufKGamdZRHT9laX59hS1rMaq1y19+0+87bsnq9qVt//pmFve6FPx6Mfs9WLqX0ITCUBBWsqHW3f9ZYFtn3hfeUy0anO6yeywObVK2WV9foMvy9toz1ypXu1+NtbaHSgrND+gT4sNNrbcwxvFb6ewdqx7WfL+l4HhkS8lMsytN/o4L6PNUcOfbLzvstXvL48tPmNPX2VK5dPl2dXvq/se/PXhgPlsuKj5bmS3Vt+odGRj0c/ZqsnGDsRmEICCtYUOti+6q0L7CqXis4OulS0cs0bx1zfaPA7df8RnTFjQXmI84sPEW7ve3uPMxn6YOI77VE5ox2Z5/f+ennW4B+M+M9LytIYa8MLiw5+o+5lL+q9VIvueqjnIer1xvKX/6Nyep+tnnHsSGAKCChYU+Ag+4q3L9B9BqAuNLpyzTf2FNy9jlY9+/LgYz887LXDH8i7sTzs+e/39B7dN0vXRTDXbXzziK99OT7seaQPerIsObG3LD3R3mbNXtY6Izf00Th/szwa59U9GU10p+4fJ2x6+B0Tuuer+566TWUh1Hnz1wz7GP04Hv2arYka25/AZBZQsCbz0fXdYgLd9wFNZAXz02Xxz/q4lfa2oPyibMOW4cXpxvVrzZPlUuL18rzAuk2fPquc6XpPTw9i7l79fE1Za2tpWXNrpO3IwU82B5//WOefVqzu7TLftasXyqXOf1IWWL3Weu1En2M4kYMx2jpY9eb2utBpe5HXmw93fmdZUX3ZROJ72rf7DFY9ZvXY9brVRxLVR+G0t/o8wpEeQt2P49Gv2erVxn4EpoKAgjUVjrLveNsC3X+g6i/DHn7Vu0dcYLT7zbrPfo11g/aest7SqbKgZXtb+8D4Z2jqIphPfeED5XEt7XvEppXPVh5GPcJaWzX34oXDzTNP/mTnPeof/frHf7zt2JE/bPbv/a+d3ZYu/4ryjMVvHe9lE/731kruT32orDv1XOe1g1dyP1YuGe4vlw7bW/1FZ/1FYX1kTXLbt/tXh6y5tbLcf3VPuQ+rl63+MOLJz/14pwjW1zz66veOuIp/v45HP2arFxv7EJgqAgrWVDnSvudtC+wrSy0cL0sutLdeVhCvN7jXNbRefD5d01qRfH55yPJI26kTW8vq7x8ZVB5WN/XSVL00Ntp2/Nhny0Onf7nzzwvK41k2bHnrmN/3mbJK+sXzL944vm7jW8qz9R4d9TX1ZvO6svrgy3MbHnxbWfhz/W27dgcMfxbhw2Utqe8aslv3oqrLymN0VpfH6SS348f+tPUw7/Y2e8695defPzjmsWjv211G6wr0Wx79oVE/Xj+OR79mK3kMZBG4kwUUrDv56PnsfRWo91LVstS5PFUWuKyPial/PEfa6pmYPTt+qTl96qnOP88vj5apZ1tG2+oz73Zs++CQS0v3rnpDs2L114/4ktaltLL/tfJsxPa2sRSf+eMUn+4/tvX5irXIzZw1fIX5mntg32+WBzD/3oRK3K0cnJuXYuvl1Ju/vquXIetDq2fMmDckrl6urIXv8uUTnf9eS9iiux6+lbcd8TVXr5xttn3xA82NcjaqvS1f8dXll4TfMuZ7XCjFtd5/1b7UW3ceb1mJfhyPfs1W7AAIInCHCyhYd/gB9PH7K3Dw+Y83Rw5+ovOmA+UPf/2Du+TuV5eHDg90/nt9pMyBstjk2TO7Ov+t3j9Uz/rMX7B2zA9dn2H4bGvdpxcfKFzP0NQ/0oOLxumTT7cWMH3x0mDTTOTesO6buOvDqOvSEINXHb9SSsbh/b9VVij/zJDv8VIs8lmXNqilqa6KfnObXry+Z9SzZPWhyzuf/lBhut7aux6LLaWM1VKW2rrPptXcRXc9UgrvN5RFYFcOeZv6a9H6rMJDxWvw8yfr+lkPvuIfDSuJ3Z+xH8ejX7OV8pdD4E4WULDu5KPns/ddoN7vVB/gPHhB0FYVKGez5pYzWfXRKPWs0uVLx4Z9ttVr/0p5xMpX9vSZDx/4RPlD/fEh+9bsOeUy1cDA7OZSyR+8Llfdsf46cVO5/DgwY25P73HlyunWGlODHwNTXzhz1pJmdnkMTb2sWe8Pap+xa4f2cmm0pw8waKfqWldNP3vmxcfM9LLWWPcN4vMXPtD6AcFYl1Qn8tnqvVS7nvn58hicvcNeVotcfVzP9PKonCulFF68cKhT9jo7l0u76zfWM2sPjvu2/Toe/Zitcb+sHQhMAQEFawocZF8xK3Dt2uVy5uhXm5PHH+8puJ65qr/qu7s8WHkiWz0TUv8YDj6TNdrr643edVmGehZqIlstUHt2frTcW3V43JfV77FyzTc19TJZejt84HdaZ37aW/21Xl3JfryiVC971VXeBz/OZrzLcRP97Fevni/3Yv1KKdVbJ/TS+szC+9Z/x4TWzurX8ejHbE0Iy84EJqGAgjUJD6qv1B+Bk+XXfkcP/f6QX7sNfud6xumush7VvavfUM4ITaz4tHPOnnm2lKzfLouc1kuNL14ybP97XRW+Lsdwz4qvGXKJciIC9SxNLTjHj/1Jc7U8d3H4Nr1ZuHhz67LYSOs4TeS9Rtr37Jndza6nP1z+6ealvroQ6+byKKHRfgXZnVEvY27f+tODPvu01lmsBYs23O5HG/L6ep/UsSOf/tLDnIcfi/bOM8qvN5cuf115oPdXNjPGeGbhaB+uX8ejH7MVPQDCCNxhAgrWHXbAfNyXn8Cli8fKTekHyr1Qp8uNzZfLH9X5rTNJ8xesjy0dcPnyyebCuefLTd0nWzdd1z/i9TLevHI/13hneXoVq5fpzp3dUy5vHm99l4GyxlQtcPU9Zs5c2GvMpN+v3mt1vh6LSy+UHxdcaK0LNlAuEdcyVR/OnVqTq1/Hox+zNemHwhckMIKAgmUsCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFvh/GqnM0YgKk+cAAAAASUVORK5CYII=">
                            </a>
                        </div>
                        <div class="bp-category-tag">RPA</div>
                        <div class="text-content">
                            <a href="" class="case-title">
                                <h3>Lorem ipsum dolor</h3>
                            </a>
                            <div class="real-content">
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia recusandae, commodi rem magni optio obcaecati
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 100px;">
                    <div class="panel wow fadeInUp" data-wow-delay="600ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a href="">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAMgCAYAAAD/YBzEAAAgAElEQVR4Xu3debCd510f8Fe62lfLkmxtliVr85q1pFAoKQUChdC0UMp0yQTakkLChACl0wChaZpkpsNSUkoaSltC02kp0EJbmoZCSKANhCWJE1uyrcWSLGuXte9rn+co5/jec7dzpa9PrXs/7z+Zsd7zPed83l/mfud93/O80175ZT9xo7ERIECAAAECBAjEBKYpWDFLQQQIECBAgACBloCCZRAIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqbmoKXLt2qTl/9rnm0qWjzbWrF5pp06Y3AwNzm9lzljVz561uBmbMvS2YK5dPNefP7Wvq/16/frmZOXNxM2vO0mbe/LXlvabdVnb7xTduXG/Ond3bXL70QnP1yplm+sDs1vvMW7C2/O/CyHtMhpDr1680F84faC5dONxcvXahuXH9auv4tqzm39fMnLUo8jX7dTz6MVsRECEE7jABBesOO2A+7stLoJaqI4c+1Zw+ua25cePaKB9uWqtkLb77seaeFX+hVb563c6e2d0cPvDbzdnTO8tLbgx72cxZdzVLl//Zkvs1zbTpA73GDtmvFobDB36nOX7sT0uxOj08o3zehYs2NytWf0MpEGtu6T0mw4suXjjUHDn4qebk8S+UY3111K9Uj/Wye7+qWbL0tbdUfvt1PPoxW5PhuPsOBG5VQMG6VTmvm9IC9Y/ggef+R/PC0c+MWHxGw3nstR9opk+f2ZPdof3/uxSfT5R9r4+7/9x5a5p1G9/czJp997j7Dt7hYjkLs2fnR5tLFw+P+7pp0waalWu+qVm+4qvH3Xey7XDk0O81h57/X2OU6OHfeMHCDc39G/52M2Pmgp45+nU8+jFbPX9pOxKYpAIK1iQ9sL7WSydw7drFZveOX2zOnXl2xDepBWratBlN3a/7rFOvBasWq0P7Pz4kf1rJnTPnnnLpbk4pREeHnW2aXf5t00Pf1/PlyCvlMuCObf+iXHY8OeR96lmx2bOXti5/XbpwZNjZmjXr/lrrrNlU2Q4f/N1WuereZsxY2Myeu7x1rOuZv1qOuo93PZu14cHvKZeL54zL1a/j0Y/ZGvfL2oHAFBBQsKbAQfYVcwL1vpjdO/5dc+bUM0NC77r7VeWS0Gua+QvXd/6Y3rh+rblYzgydK5f5Tp14sjl7Zlfz2GvfP+4ZrDOndjTPbv+FIX+s6yWne1d9XTNjxvzW+964caN8hqebfXt+bUjRWnzXo826TW/p6QvvfOpD5Z6r3Z19Z81a0qxZ9+3NwsWbOv+t/tE/tP+3muNH/6jz3+qZrI0Pvb11v9Fk3+plwWe2/vMC/uJZxDlzVzWr739Ts2DhA0O+/pUrZ8slxN9tjh3+v0OO3fIVr29W3ffGcan6cTz6NVvjflk7EJgCAgrWFDjIvmJO4NiRP2j27/31TmC9kX3dxrc0CxZtGPdN6lmnWeXM0Fj3YNXitGPbB8tN1Ps7efeu+vpy/9MbRsy/fOl4s33rB8vZsvOdf9/44NtL0Vs35uc5dWJruTT4kc4+M2YuajY//I5yg/biEV9XL4cePfz7nX9bUO7J2rDlu8f9znf6Dvt2/0q5N+1POl+jlqtaLgcGZo361Y6WgnXguf82qJDOaB559XvKa2aP+pp+HI9+zdadfsx9fgIpAQUrJSln0gvUX1s9/eRPNtdbl/6a1qWhjQ+9LXomp/sP7Zx5q0rx+f4xS1ktALUItLcFizaV8vPWMY9HPStzsfwSrr3dv+HNzV13v2LU11wvv5TbvvWnW5cm29vGB9/WOmM3mbetj7+39YvK9vbA5rcOOcM30nevRWZ79b1wsPPP6zZ+Z7N4ySOjUvXjePRrtibzPPhuBCYioGBNRMu+U1qg+yzOWGeWbhVqz67/0Jwqv1Jrb2vX/41mybLXjBlXL1tu+8L7B10qnNY8/Kp3j7q0Qr1X6JlSFNvbrNnLmgcf+4fj/uKt++zd0uVfUS4pfuutftWX/etqqXzis+/qfM7p02c3j77mvT39CvTg8x8vlwvrDxRubqvu+5ZRfxzQr+PRj9l62R9UH5BAHwUUrD5ie6s7V6DeT7XtC/+0uXr1XOtL1D+2D7/qx8e8VDTRb1vf48nH39M5QzZ9+qzWpaVefnV4YN9vNkfLL93a21g3oh85+Mnm4PMf6+xbl1+o93eNt9X1verna9+PVC8nPvzKHxvvZXfsv1+9er7Z+vl/3Pn8s+csbxXRXrYXjnymeX7vf+nsWn2r80hbP45Hv2arFxv7EJgqAgrWVDnSvudtCdSb1Pfs/KVOxt3LXtfct/7bbyuz+8X1ZvidT3+o85/rz/zrL9B62eo6XPWXje1t8d2vbNaVJQJG2nY986/Lulo7Ov+04cHvHXbD9mjvWe/3unD++c4/b3n0HzRz5t7by0ccc596I31di6u9LVy0pVm/+e+Oe1at7t9dZuov9zaWX1NOnz7jtj5XPTP4xGd/tPMryvrryodf+aM9ZR49/H/KfVj/vbPvqvv+cjmD9ef/vx2Pfs1WTzh2IjBFBBSsKXKgfc3bE9hf/lgeK38029t49yzdyrt1F4V7Vv7Fsu7UX+opqp5Z2/r593T2raWnlp+Rtq2PlzNxnQVFp5dfNr6vp7NkNWv/3t9ojh35dNyhlpldz/z8kKUvVpTvfm8xGGu7cP5gs+Opny0n1a60dqtnFjc/8v1lBf3lPbmNt9POp//VoM80rXmknLXsZV2r55795ebEC5/txG8o96stGOV+tX4cj37N1nie/p3AVBJQsKbS0fZdb1lg51M/V5Y02NN5fb00Vi+R1bWuTr7weHPi+OPlETPHSnE523pETv23egZqcblxfP6C+3t63+57vOqSCUuXv66n19adnvjcu4fcgF+XhOj+xWL9vE+W/drbRC/zHT1Uzszse/HMzIrV31guL35tz59xrB3rjwjqzeHty7Blafpm45bvHfUXkdevXW62l19cXrp4pBO79oFyz1pZLiO1nXjh881zz/7HTlwv9921fgzxxD8rjzS6WfrqL0dv3uM2fAX/fh2PfsxWylwOgckioGBNliPpe7xkAvVXYU9+7sdazwCsWy1Q9Wbn02UtrH27//OQX5mN9CEWLn6w3Az+bc2scolprG33jo+UR+5s7ezywOa/V36xtqXn7/XMkz9Vfrl2qLP/Q6/4kfLHfcmQ19flH7Zv/ZnOf5tXyl9dnLTX7eTxLzZ7d320s/uSZX+mWbv+O3p9+bj7nT75dGudsfaCnfWy3OZHfqCs/zVv2GufK7+cPDFoCYW7l31ZuWz718d9j4nsUI/97u3/tjlz+kvrnpWSdP8Df2vUX1xeKWcGd2//xSGXUcf6BWG/jkc/ZmsirvYlMBUEFKypcJR9x9sS6D7LUC+/3bPya8uZjf/UKQLjvUFdZ6oWprnzVo66686nP1wuR+3q/PvmR97ZeoZhr9uu8vq6mGl72/zIDw57v7Ond5VLcR/u7LPorkea9Zu+s9e3KPnPNrvKZbP2NpGFTXt9kwP7/me5Yf9TY37GEy987kv+N3erq9jX5Symj7E+Va/v371ffZD33vLrzrqwa3urxXfxkle0LkXWe73qgqznim1dMuNaWQH/5jatXOL95jIrrx/1rft1PPoxW7fq63UEJquAgjVZj6zvFRO4fOlk89QX39/JmzlzcesyVvuBv/WPbD17UstQfZzNlUsnmlMnn2yt3j740Sn1OYGbH37nqI+yGX4D+Q+XG8jv6fl7PFvPtAwqASMtOHr65FNfOkN0M/aucjN8fV5er9v5c/taj9dpby/FgqP1odn13qfzZ/d23mfV2nKT+L03bxKva3FVq+vXL92sMcV800PvKP4rev0aE96vnsk6deKLrVXaz7U+1/AHbw8OrbOw8r5vLg/JfnFV/JHetF/Hox+zNWFULyAwyQUUrEl+gH292xe4cP5Q+YP+UyMGLV3+5c2qtW8a8RdrZ05tb/3ysH1psQbcXZ7hd195lt9I29NP/MSQ+4keesW7JvTw5j07/30pAU90oh/Y/N3lEuPmIW91sqyxVc/GtLclS8slvgd6v8RXbyqvC462t3kL1pVy8/bbR+5KuFxKar0fq3026Objeb6vFM4Vzc6n/uWQle7X3P9tzdJ7vjz+GQYH1pvwq93xo3/cOos31gO46yOE6r1a9dLwtGnTxvxc/Toe/Zitl/QACCdwBwooWHfgQfOR+yvQXSra717/gD5QlhIYa+u+Z6mu/v7QK99VFgFdNOxlw/8IDr+Haqz3Gl6wht/DNewP+gTvoeoumxO9h2siR657aYx6s3j94cDxY3/ciRlrOYqJvNdY+9bHEdVSWs/eTWSbM3dlq7yOdZm3X8ejH7M1ERv7EpgKAgrWVDjKvuNtCVy6eKz1q7DubcujvV3C636I7+r7/2qz7J4/NyyvH5dx+nVJ6rbAB724e1mIwbm1cNX71AYG5qTebljO5csnyxmzn2uulP9tb/WB2/Xh24sWP9TMmlOfLTmjXDI+W7tFuM0AAB0TSURBVC5pPte8cPQPyxpjOzv71suX6zf9nXKpcOOIn7Ffx6Mfs/WSHQTBBO5QAQXrDj1wPnb/BOoNzNvKM+kGb3PnrWmtt9TL9sLRP2qe3/NrnV1Hu+9p6JpLTas8TMWb3Aeb1sfV1IIzeHHT+u83Lxm+PfocyJGO5bPP/EL5BeH2zj/Nm7+2Wbfpu8oZyAWjHvpjhz/d7H/uNzr/PjAwr9ny2A+NeNayfze5D17P66WZrV7+v2AfAlNJQMGaSkfbd70lgbqe0ROf/ZEhr11azkCtKWeietm6LzHOmbuqLAL6A8NeWldiryuyt7eR7qEa6/16WqbhXFmmYdvgZRomdg/VS71Mw0jfry6HsXv7vxnyT2M9eqaXY9LLPufKGamdZRHT9laX59hS1rMaq1y19+0+87bsnq9qVt//pmFve6FPx6Mfs9WLqX0ITCUBBWsqHW3f9ZYFtn3hfeUy0anO6yeywObVK2WV9foMvy9toz1ypXu1+NtbaHSgrND+gT4sNNrbcwxvFb6ewdqx7WfL+l4HhkS8lMsytN/o4L6PNUcOfbLzvstXvL48tPmNPX2VK5dPl2dXvq/se/PXhgPlsuKj5bmS3Vt+odGRj0c/ZqsnGDsRmEICCtYUOti+6q0L7CqXis4OulS0cs0bx1zfaPA7df8RnTFjQXmI84sPEW7ve3uPMxn6YOI77VE5ox2Z5/f+ennW4B+M+M9LytIYa8MLiw5+o+5lL+q9VIvueqjnIer1xvKX/6Nyep+tnnHsSGAKCChYU+Ag+4q3L9B9BqAuNLpyzTf2FNy9jlY9+/LgYz887LXDH8i7sTzs+e/39B7dN0vXRTDXbXzziK99OT7seaQPerIsObG3LD3R3mbNXtY6Izf00Th/szwa59U9GU10p+4fJ2x6+B0Tuuer+566TWUh1Hnz1wz7GP04Hv2arYka25/AZBZQsCbz0fXdYgLd9wFNZAXz02Xxz/q4lfa2oPyibMOW4cXpxvVrzZPlUuL18rzAuk2fPquc6XpPTw9i7l79fE1Za2tpWXNrpO3IwU82B5//WOefVqzu7TLftasXyqXOf1IWWL3Weu1En2M4kYMx2jpY9eb2utBpe5HXmw93fmdZUX3ZROJ72rf7DFY9ZvXY9brVRxLVR+G0t/o8wpEeQt2P49Gv2erVxn4EpoKAgjUVjrLveNsC3X+g6i/DHn7Vu0dcYLT7zbrPfo11g/aest7SqbKgZXtb+8D4Z2jqIphPfeED5XEt7XvEppXPVh5GPcJaWzX34oXDzTNP/mTnPeof/frHf7zt2JE/bPbv/a+d3ZYu/4ryjMVvHe9lE/731kruT32orDv1XOe1g1dyP1YuGe4vlw7bW/1FZ/1FYX1kTXLbt/tXh6y5tbLcf3VPuQ+rl63+MOLJz/14pwjW1zz66veOuIp/v45HP2arFxv7EJgqAgrWVDnSvudtC+wrSy0cL0sutLdeVhCvN7jXNbRefD5d01qRfH55yPJI26kTW8vq7x8ZVB5WN/XSVL00Ntp2/Nhny0Onf7nzzwvK41k2bHnrmN/3mbJK+sXzL944vm7jW8qz9R4d9TX1ZvO6svrgy3MbHnxbWfhz/W27dgcMfxbhw2Utqe8aslv3oqrLymN0VpfH6SS348f+tPUw7/Y2e8695defPzjmsWjv211G6wr0Wx79oVE/Xj+OR79mK3kMZBG4kwUUrDv56PnsfRWo91LVstS5PFUWuKyPial/PEfa6pmYPTt+qTl96qnOP88vj5apZ1tG2+oz73Zs++CQS0v3rnpDs2L114/4ktaltLL/tfJsxPa2sRSf+eMUn+4/tvX5irXIzZw1fIX5mntg32+WBzD/3oRK3K0cnJuXYuvl1Ju/vquXIetDq2fMmDckrl6urIXv8uUTnf9eS9iiux6+lbcd8TVXr5xttn3xA82NcjaqvS1f8dXll4TfMuZ7XCjFtd5/1b7UW3ceb1mJfhyPfs1W7AAIInCHCyhYd/gB9PH7K3Dw+Y83Rw5+ovOmA+UPf/2Du+TuV5eHDg90/nt9pMyBstjk2TO7Ov+t3j9Uz/rMX7B2zA9dn2H4bGvdpxcfKFzP0NQ/0oOLxumTT7cWMH3x0mDTTOTesO6buOvDqOvSEINXHb9SSsbh/b9VVij/zJDv8VIs8lmXNqilqa6KfnObXry+Z9SzZPWhyzuf/lBhut7aux6LLaWM1VKW2rrPptXcRXc9UgrvN5RFYFcOeZv6a9H6rMJDxWvw8yfr+lkPvuIfDSuJ3Z+xH8ejX7OV8pdD4E4WULDu5KPns/ddoN7vVB/gPHhB0FYVKGez5pYzWfXRKPWs0uVLx4Z9ttVr/0p5xMpX9vSZDx/4RPlD/fEh+9bsOeUy1cDA7OZSyR+8Llfdsf46cVO5/DgwY25P73HlyunWGlODHwNTXzhz1pJmdnkMTb2sWe8Pap+xa4f2cmm0pw8waKfqWldNP3vmxcfM9LLWWPcN4vMXPtD6AcFYl1Qn8tnqvVS7nvn58hicvcNeVotcfVzP9PKonCulFF68cKhT9jo7l0u76zfWM2sPjvu2/Toe/Zitcb+sHQhMAQEFawocZF8xK3Dt2uVy5uhXm5PHH+8puJ65qr/qu7s8WHkiWz0TUv8YDj6TNdrr643edVmGehZqIlstUHt2frTcW3V43JfV77FyzTc19TJZejt84HdaZ37aW/21Xl3JfryiVC971VXeBz/OZrzLcRP97Fevni/3Yv1KKdVbJ/TS+szC+9Z/x4TWzurX8ejHbE0Iy84EJqGAgjUJD6qv1B+Bk+XXfkcP/f6QX7sNfud6xumush7VvavfUM4ITaz4tHPOnnm2lKzfLouc1kuNL14ybP97XRW+Lsdwz4qvGXKJciIC9SxNLTjHj/1Jc7U8d3H4Nr1ZuHhz67LYSOs4TeS9Rtr37Jndza6nP1z+6ealvroQ6+byKKHRfgXZnVEvY27f+tODPvu01lmsBYs23O5HG/L6ep/UsSOf/tLDnIcfi/bOM8qvN5cuf115oPdXNjPGeGbhaB+uX8ejH7MVPQDCCNxhAgrWHXbAfNyXn8Cli8fKTekHyr1Qp8uNzZfLH9X5rTNJ8xesjy0dcPnyyebCuefLTd0nWzdd1z/i9TLevHI/13hneXoVq5fpzp3dUy5vHm99l4GyxlQtcPU9Zs5c2GvMpN+v3mt1vh6LSy+UHxdcaK0LNlAuEdcyVR/OnVqTq1/Hox+zNemHwhckMIKAgmUsCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFvh/GqnM0YgKk+cAAAAASUVORK5CYII=">
                            </a>
                        </div>
                        <div class="bp-category-tag">Market & Business intelligence</div>
                        <div class="text-content">
                            <a href="" class="case-title">
                                <h3>Lorem ipsum dolor</h3>
                            </a>
                            <div class="real-content">
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia recusandae, commodi rem magni optio obcaecati
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 150px;">
                    <div class="panel wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a href="">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAMgCAYAAAD/YBzEAAAgAElEQVR4Xu3debCd510f8Fe62lfLkmxtliVr85q1pFAoKQUChdC0UMp0yQTakkLChACl0wChaZpkpsNSUkoaSltC02kp0EJbmoZCSKANhCWJE1uyrcWSLGuXte9rn+co5/jec7dzpa9PrXs/7z+Zsd7zPed83l/mfud93/O80175ZT9xo7ERIECAAAECBAjEBKYpWDFLQQQIECBAgACBloCCZRAIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqbmoKXLt2qTl/9rnm0qWjzbWrF5pp06Y3AwNzm9lzljVz561uBmbMvS2YK5dPNefP7Wvq/16/frmZOXNxM2vO0mbe/LXlvabdVnb7xTduXG/Ond3bXL70QnP1yplm+sDs1vvMW7C2/O/CyHtMhpDr1680F84faC5dONxcvXahuXH9auv4tqzm39fMnLUo8jX7dTz6MVsRECEE7jABBesOO2A+7stLoJaqI4c+1Zw+ua25cePaKB9uWqtkLb77seaeFX+hVb563c6e2d0cPvDbzdnTO8tLbgx72cxZdzVLl//Zkvs1zbTpA73GDtmvFobDB36nOX7sT0uxOj08o3zehYs2NytWf0MpEGtu6T0mw4suXjjUHDn4qebk8S+UY3111K9Uj/Wye7+qWbL0tbdUfvt1PPoxW5PhuPsOBG5VQMG6VTmvm9IC9Y/ggef+R/PC0c+MWHxGw3nstR9opk+f2ZPdof3/uxSfT5R9r4+7/9x5a5p1G9/czJp997j7Dt7hYjkLs2fnR5tLFw+P+7pp0waalWu+qVm+4qvH3Xey7XDk0O81h57/X2OU6OHfeMHCDc39G/52M2Pmgp45+nU8+jFbPX9pOxKYpAIK1iQ9sL7WSydw7drFZveOX2zOnXl2xDepBWratBlN3a/7rFOvBasWq0P7Pz4kf1rJnTPnnnLpbk4pREeHnW2aXf5t00Pf1/PlyCvlMuCObf+iXHY8OeR96lmx2bOXti5/XbpwZNjZmjXr/lrrrNlU2Q4f/N1WuereZsxY2Myeu7x1rOuZv1qOuo93PZu14cHvKZeL54zL1a/j0Y/ZGvfL2oHAFBBQsKbAQfYVcwL1vpjdO/5dc+bUM0NC77r7VeWS0Gua+QvXd/6Y3rh+rblYzgydK5f5Tp14sjl7Zlfz2GvfP+4ZrDOndjTPbv+FIX+s6yWne1d9XTNjxvzW+964caN8hqebfXt+bUjRWnzXo826TW/p6QvvfOpD5Z6r3Z19Z81a0qxZ9+3NwsWbOv+t/tE/tP+3muNH/6jz3+qZrI0Pvb11v9Fk3+plwWe2/vMC/uJZxDlzVzWr739Ts2DhA0O+/pUrZ8slxN9tjh3+v0OO3fIVr29W3ffGcan6cTz6NVvjflk7EJgCAgrWFDjIvmJO4NiRP2j27/31TmC9kX3dxrc0CxZtGPdN6lmnWeXM0Fj3YNXitGPbB8tN1Ps7efeu+vpy/9MbRsy/fOl4s33rB8vZsvOdf9/44NtL0Vs35uc5dWJruTT4kc4+M2YuajY//I5yg/biEV9XL4cePfz7nX9bUO7J2rDlu8f9znf6Dvt2/0q5N+1POl+jlqtaLgcGZo361Y6WgnXguf82qJDOaB559XvKa2aP+pp+HI9+zdadfsx9fgIpAQUrJSln0gvUX1s9/eRPNtdbl/6a1qWhjQ+9LXomp/sP7Zx5q0rx+f4xS1ktALUItLcFizaV8vPWMY9HPStzsfwSrr3dv+HNzV13v2LU11wvv5TbvvWnW5cm29vGB9/WOmM3mbetj7+39YvK9vbA5rcOOcM30nevRWZ79b1wsPPP6zZ+Z7N4ySOjUvXjePRrtibzPPhuBCYioGBNRMu+U1qg+yzOWGeWbhVqz67/0Jwqv1Jrb2vX/41mybLXjBlXL1tu+8L7B10qnNY8/Kp3j7q0Qr1X6JlSFNvbrNnLmgcf+4fj/uKt++zd0uVfUS4pfuutftWX/etqqXzis+/qfM7p02c3j77mvT39CvTg8x8vlwvrDxRubqvu+5ZRfxzQr+PRj9l62R9UH5BAHwUUrD5ie6s7V6DeT7XtC/+0uXr1XOtL1D+2D7/qx8e8VDTRb1vf48nH39M5QzZ9+qzWpaVefnV4YN9vNkfLL93a21g3oh85+Mnm4PMf6+xbl1+o93eNt9X1verna9+PVC8nPvzKHxvvZXfsv1+9er7Z+vl/3Pn8s+csbxXRXrYXjnymeX7vf+nsWn2r80hbP45Hv2arFxv7EJgqAgrWVDnSvudtCdSb1Pfs/KVOxt3LXtfct/7bbyuz+8X1ZvidT3+o85/rz/zrL9B62eo6XPWXje1t8d2vbNaVJQJG2nY986/Lulo7Ov+04cHvHXbD9mjvWe/3unD++c4/b3n0HzRz5t7by0ccc596I31di6u9LVy0pVm/+e+Oe1at7t9dZuov9zaWX1NOnz7jtj5XPTP4xGd/tPMryvrryodf+aM9ZR49/H/KfVj/vbPvqvv+cjmD9ef/vx2Pfs1WTzh2IjBFBBSsKXKgfc3bE9hf/lgeK38029t49yzdyrt1F4V7Vv7Fsu7UX+opqp5Z2/r593T2raWnlp+Rtq2PlzNxnQVFp5dfNr6vp7NkNWv/3t9ojh35dNyhlpldz/z8kKUvVpTvfm8xGGu7cP5gs+Opny0n1a60dqtnFjc/8v1lBf3lPbmNt9POp//VoM80rXmknLXsZV2r55795ebEC5/txG8o96stGOV+tX4cj37N1nie/p3AVBJQsKbS0fZdb1lg51M/V5Y02NN5fb00Vi+R1bWuTr7weHPi+OPlETPHSnE523pETv23egZqcblxfP6C+3t63+57vOqSCUuXv66n19adnvjcu4fcgF+XhOj+xWL9vE+W/drbRC/zHT1Uzszse/HMzIrV31guL35tz59xrB3rjwjqzeHty7Blafpm45bvHfUXkdevXW62l19cXrp4pBO79oFyz1pZLiO1nXjh881zz/7HTlwv9921fgzxxD8rjzS6WfrqL0dv3uM2fAX/fh2PfsxWylwOgckioGBNliPpe7xkAvVXYU9+7sdazwCsWy1Q9Wbn02UtrH27//OQX5mN9CEWLn6w3Az+bc2scolprG33jo+UR+5s7ezywOa/V36xtqXn7/XMkz9Vfrl2qLP/Q6/4kfLHfcmQ19flH7Zv/ZnOf5tXyl9dnLTX7eTxLzZ7d320s/uSZX+mWbv+O3p9+bj7nT75dGudsfaCnfWy3OZHfqCs/zVv2GufK7+cPDFoCYW7l31ZuWz718d9j4nsUI/97u3/tjlz+kvrnpWSdP8Df2vUX1xeKWcGd2//xSGXUcf6BWG/jkc/ZmsirvYlMBUEFKypcJR9x9sS6D7LUC+/3bPya8uZjf/UKQLjvUFdZ6oWprnzVo66686nP1wuR+3q/PvmR97ZeoZhr9uu8vq6mGl72/zIDw57v7Ond5VLcR/u7LPorkea9Zu+s9e3KPnPNrvKZbP2NpGFTXt9kwP7/me5Yf9TY37GEy987kv+N3erq9jX5Symj7E+Va/v371ffZD33vLrzrqwa3urxXfxkle0LkXWe73qgqznim1dMuNaWQH/5jatXOL95jIrrx/1rft1PPoxW7fq63UEJquAgjVZj6zvFRO4fOlk89QX39/JmzlzcesyVvuBv/WPbD17UstQfZzNlUsnmlMnn2yt3j740Sn1OYGbH37nqI+yGX4D+Q+XG8jv6fl7PFvPtAwqASMtOHr65FNfOkN0M/aucjN8fV5er9v5c/taj9dpby/FgqP1odn13qfzZ/d23mfV2nKT+L03bxKva3FVq+vXL92sMcV800PvKP4rev0aE96vnsk6deKLrVXaz7U+1/AHbw8OrbOw8r5vLg/JfnFV/JHetF/Hox+zNWFULyAwyQUUrEl+gH292xe4cP5Q+YP+UyMGLV3+5c2qtW8a8RdrZ05tb/3ysH1psQbcXZ7hd195lt9I29NP/MSQ+4keesW7JvTw5j07/30pAU90oh/Y/N3lEuPmIW91sqyxVc/GtLclS8slvgd6v8RXbyqvC462t3kL1pVy8/bbR+5KuFxKar0fq3026Objeb6vFM4Vzc6n/uWQle7X3P9tzdJ7vjz+GQYH1pvwq93xo3/cOos31gO46yOE6r1a9dLwtGnTxvxc/Toe/Zitl/QACCdwBwooWHfgQfOR+yvQXSra717/gD5QlhIYa+u+Z6mu/v7QK99VFgFdNOxlw/8IDr+Haqz3Gl6wht/DNewP+gTvoeoumxO9h2siR657aYx6s3j94cDxY3/ciRlrOYqJvNdY+9bHEdVSWs/eTWSbM3dlq7yOdZm3X8ejH7M1ERv7EpgKAgrWVDjKvuNtCVy6eKz1q7DubcujvV3C636I7+r7/2qz7J4/NyyvH5dx+nVJ6rbAB724e1mIwbm1cNX71AYG5qTebljO5csnyxmzn2uulP9tb/WB2/Xh24sWP9TMmlOfLTmjXDI+W7tFuM0AAB0TSURBVC5pPte8cPQPyxpjOzv71suX6zf9nXKpcOOIn7Ffx6Mfs/WSHQTBBO5QAQXrDj1wPnb/BOoNzNvKM+kGb3PnrWmtt9TL9sLRP2qe3/NrnV1Hu+9p6JpLTas8TMWb3Aeb1sfV1IIzeHHT+u83Lxm+PfocyJGO5bPP/EL5BeH2zj/Nm7+2Wbfpu8oZyAWjHvpjhz/d7H/uNzr/PjAwr9ny2A+NeNayfze5D17P66WZrV7+v2AfAlNJQMGaSkfbd70lgbqe0ROf/ZEhr11azkCtKWeietm6LzHOmbuqLAL6A8NeWldiryuyt7eR7qEa6/16WqbhXFmmYdvgZRomdg/VS71Mw0jfry6HsXv7vxnyT2M9eqaXY9LLPufKGamdZRHT9laX59hS1rMaq1y19+0+87bsnq9qVt//pmFve6FPx6Mfs9WLqX0ITCUBBWsqHW3f9ZYFtn3hfeUy0anO6yeywObVK2WV9foMvy9toz1ypXu1+NtbaHSgrND+gT4sNNrbcwxvFb6ewdqx7WfL+l4HhkS8lMsytN/o4L6PNUcOfbLzvstXvL48tPmNPX2VK5dPl2dXvq/se/PXhgPlsuKj5bmS3Vt+odGRj0c/ZqsnGDsRmEICCtYUOti+6q0L7CqXis4OulS0cs0bx1zfaPA7df8RnTFjQXmI84sPEW7ve3uPMxn6YOI77VE5ox2Z5/f+ennW4B+M+M9LytIYa8MLiw5+o+5lL+q9VIvueqjnIer1xvKX/6Nyep+tnnHsSGAKCChYU+Ag+4q3L9B9BqAuNLpyzTf2FNy9jlY9+/LgYz887LXDH8i7sTzs+e/39B7dN0vXRTDXbXzziK99OT7seaQPerIsObG3LD3R3mbNXtY6Izf00Th/szwa59U9GU10p+4fJ2x6+B0Tuuer+566TWUh1Hnz1wz7GP04Hv2arYka25/AZBZQsCbz0fXdYgLd9wFNZAXz02Xxz/q4lfa2oPyibMOW4cXpxvVrzZPlUuL18rzAuk2fPquc6XpPTw9i7l79fE1Za2tpWXNrpO3IwU82B5//WOefVqzu7TLftasXyqXOf1IWWL3Weu1En2M4kYMx2jpY9eb2utBpe5HXmw93fmdZUX3ZROJ72rf7DFY9ZvXY9brVRxLVR+G0t/o8wpEeQt2P49Gv2erVxn4EpoKAgjUVjrLveNsC3X+g6i/DHn7Vu0dcYLT7zbrPfo11g/aest7SqbKgZXtb+8D4Z2jqIphPfeED5XEt7XvEppXPVh5GPcJaWzX34oXDzTNP/mTnPeof/frHf7zt2JE/bPbv/a+d3ZYu/4ryjMVvHe9lE/731kruT32orDv1XOe1g1dyP1YuGe4vlw7bW/1FZ/1FYX1kTXLbt/tXh6y5tbLcf3VPuQ+rl63+MOLJz/14pwjW1zz66veOuIp/v45HP2arFxv7EJgqAgrWVDnSvudtC+wrSy0cL0sutLdeVhCvN7jXNbRefD5d01qRfH55yPJI26kTW8vq7x8ZVB5WN/XSVL00Ntp2/Nhny0Onf7nzzwvK41k2bHnrmN/3mbJK+sXzL944vm7jW8qz9R4d9TX1ZvO6svrgy3MbHnxbWfhz/W27dgcMfxbhw2Utqe8aslv3oqrLymN0VpfH6SS348f+tPUw7/Y2e8695defPzjmsWjv211G6wr0Wx79oVE/Xj+OR79mK3kMZBG4kwUUrDv56PnsfRWo91LVstS5PFUWuKyPial/PEfa6pmYPTt+qTl96qnOP88vj5apZ1tG2+oz73Zs++CQS0v3rnpDs2L114/4ktaltLL/tfJsxPa2sRSf+eMUn+4/tvX5irXIzZw1fIX5mntg32+WBzD/3oRK3K0cnJuXYuvl1Ju/vquXIetDq2fMmDckrl6urIXv8uUTnf9eS9iiux6+lbcd8TVXr5xttn3xA82NcjaqvS1f8dXll4TfMuZ7XCjFtd5/1b7UW3ceb1mJfhyPfs1W7AAIInCHCyhYd/gB9PH7K3Dw+Y83Rw5+ovOmA+UPf/2Du+TuV5eHDg90/nt9pMyBstjk2TO7Ov+t3j9Uz/rMX7B2zA9dn2H4bGvdpxcfKFzP0NQ/0oOLxumTT7cWMH3x0mDTTOTesO6buOvDqOvSEINXHb9SSsbh/b9VVij/zJDv8VIs8lmXNqilqa6KfnObXry+Z9SzZPWhyzuf/lBhut7aux6LLaWM1VKW2rrPptXcRXc9UgrvN5RFYFcOeZv6a9H6rMJDxWvw8yfr+lkPvuIfDSuJ3Z+xH8ejX7OV8pdD4E4WULDu5KPns/ddoN7vVB/gPHhB0FYVKGez5pYzWfXRKPWs0uVLx4Z9ttVr/0p5xMpX9vSZDx/4RPlD/fEh+9bsOeUy1cDA7OZSyR+8Llfdsf46cVO5/DgwY25P73HlyunWGlODHwNTXzhz1pJmdnkMTb2sWe8Pap+xa4f2cmm0pw8waKfqWldNP3vmxcfM9LLWWPcN4vMXPtD6AcFYl1Qn8tnqvVS7nvn58hicvcNeVotcfVzP9PKonCulFF68cKhT9jo7l0u76zfWM2sPjvu2/Toe/Zitcb+sHQhMAQEFawocZF8xK3Dt2uVy5uhXm5PHH+8puJ65qr/qu7s8WHkiWz0TUv8YDj6TNdrr643edVmGehZqIlstUHt2frTcW3V43JfV77FyzTc19TJZejt84HdaZ37aW/21Xl3JfryiVC971VXeBz/OZrzLcRP97Fevni/3Yv1KKdVbJ/TS+szC+9Z/x4TWzurX8ejHbE0Iy84EJqGAgjUJD6qv1B+Bk+XXfkcP/f6QX7sNfud6xumush7VvavfUM4ITaz4tHPOnnm2lKzfLouc1kuNL14ybP97XRW+Lsdwz4qvGXKJciIC9SxNLTjHj/1Jc7U8d3H4Nr1ZuHhz67LYSOs4TeS9Rtr37Jndza6nP1z+6ealvroQ6+byKKHRfgXZnVEvY27f+tODPvu01lmsBYs23O5HG/L6ep/UsSOf/tLDnIcfi/bOM8qvN5cuf115oPdXNjPGeGbhaB+uX8ejH7MVPQDCCNxhAgrWHXbAfNyXn8Cli8fKTekHyr1Qp8uNzZfLH9X5rTNJ8xesjy0dcPnyyebCuefLTd0nWzdd1z/i9TLevHI/13hneXoVq5fpzp3dUy5vHm99l4GyxlQtcPU9Zs5c2GvMpN+v3mt1vh6LSy+UHxdcaK0LNlAuEdcyVR/OnVqTq1/Hox+zNemHwhckMIKAgmUsCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFlCwwqDiCBAgQIAAAQIKlhkgQIAAAQIECIQFFKwwqDgCBAgQIECAgIJlBggQIECAAAECYQEFKwwqjgABAgQIECCgYJkBAgQIECBAgEBYQMEKg4ojQIAAAQIECChYZoAAAQIECBAgEBZQsMKg4ggQIECAAAECCpYZIECAAAECBAiEBRSsMKg4AgQIECBAgICCZQYIECBAgAABAmEBBSsMKo4AAQIECBAgoGCZAQIECBAgQIBAWEDBCoOKI0CAAAECBAgoWGaAAAECBAgQIBAWULDCoOIIECBAgAABAgqWGSBAgAABAgQIhAUUrDCoOAIECBAgQICAgmUGCBAgQIAAAQJhAQUrDCqOAAECBAgQIKBgmQECBAgQIECAQFhAwQqDiiNAgAABAgQIKFhmgAABAgQIECAQFvh/GqnM0YgKk+cAAAAASUVORK5CYII=">
                            </a>
                        </div>
                        <div class="bp-category-tag">Conduite du Changement </div>
                        <div class="text-content">
                            <a href="" class="case-title">
                                <h3>Lorem ipsum dolor</h3>
                            </a>
                            <div class="real-content">
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia recusandae, commodi rem magni optio obcaecati
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>





    </div>
    </section>

    <!--Reasons Start-->
    <section class="reasons">
        <div class="container">
            <div class="reasons-bg"></div>
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="reasons__left">
                        <h2 class="reasons__title">Pourquoi nous choisir ?</h2>
                        <ul class="list-unstyled reasons__list-box">
                            <li>
                                <div class="reasons__icon">
                                    <span class="icon-tick"></span>
                                </div>
                                <div class="reasons__content">
                                    <h4 class="reasons__content-title">Une équipe professionnelle</h4>
                                    <p class="reasons__content-text">Akili, c'est une équipe jeune, dynamique et surtout professionnelle</p>
                                </div>
                            </li>
                            <li>
                                <div class="reasons__icon">
                                    <span class="icon-tick"></span>
                                </div>
                                <div class="reasons__content">
                                    <h4 class="reasons__content-title"> Une expérience dans le domaine </h4>
                                    <p class="reasons__content-text"> A travers nos projets pour nos illustres partenaires, nous avons acquis une expérience précieuse dans notre domaine</p>
                                </div>
                            </li>
                            <li>
                                <div class="reasons__icon">
                                    <span class="icon-tick"></span>
                                </div>
                                <div class="reasons__content">
                                    <h4 class="reasons__content-title"> Une entreprise à l'écoute </h4>
                                    <p class="reasons__content-text">Travailler avec Akili, c'est travailler avec une entreprise à l'écoute, et toujours disponible</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6">
                    <div class="reasons__img-box">
                        <div class="reasons__img">
                            <img src="assets/images/resources/reason.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Brand One Start-->

    <!-- Expiremtal section -->
    <section class="mt-5">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h2 class="section-title__title">Nos cas clients</h2>
                <span class="section-title__tagline"> Profitez de notre expertise et de notre professionnalisme</span>
            </div>

            <!--Site Footer One Start-->
            <?php include("assets/includes/cas.php")   ?>
            <!--Site cookie One Start-->

        </div>
    </section>


    <!-- Expiremtal section End-->
    <section class="brand-one">
        <div class="container">
            <h4 class="brand-one__title"> Rencontrez nos partenaires</h4>
            <div class="thm-swiper__slider swiper-container" data-swiper-options='{"spaceBetween": 100, "slidesPerView": 5, "autoplay": { "delay": 5000 }, "breakpoints": {
                    "0": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "375": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "575": {
                        "spaceBetween": 30,
                        "slidesPerView": 3
                    },
                    "767": {
                        "spaceBetween": 50,
                        "slidesPerView": 4
                    },
                    "991": {
                        "spaceBetween": 50,
                        "slidesPerView": 5
                    },
                    "1199": {
                        "spaceBetween": 100,
                        "slidesPerView": 5
                    }
                }}'>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="assets/images/gtBank.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/sunLog.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/images/leadLogo.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/nsiaLog.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/om.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/momo.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/beninlog.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/badLog.png" alt="">
                    </div><!-- /.swiper-slide -->
                    <div class="swiper-slide">
                        <img src="assets/images/fnmlog.png" alt="">
                    </div><!-- /.swiper-slide -->
                </div>
            </div>
        </div>
    </section>
    <!--Brand One End-->
    <section class="testimonials-one">
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <div class="testimonials-one__left">
                        <div class="section-title text-left">
                            <h2 class="section-title__title">Ce qu'ils pensent de nous</h2>
                            <span class="section-title__tagline">Ils sont nombreux à nous avoir fait confiance</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="testimonials-one__right">
                        <div class="testimonials-one__carousel owl-theme owl-carousel">
                            <!--Testimonials One Single-->
                            <div class="testimonials-one__single">
                                <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                                <div class="testimonials-one__client-info">
                                    <h5 class="testimonials-one__client-name"> MTN </h5>
                                    <p class="testimonials-one__client-title">Client</p>
                                </div>
                                <div class="testimonials-one__client-img">
                                    <img src="assets/images/testimonial/testim4.jpg" alt="">
                                </div>
                                <div class="testimonials-one__quote"></div>
                            </div>
                            <!--Testimonials One Single-->
                            <div class="testimonials-one__single">
                                <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                                <div class="testimonials-one__client-info">
                                    <h5 class="testimonials-one__client-name"> Orange Côte D'ivoire </h5>
                                    <p class="testimonials-one__client-title">Client</p>
                                </div>
                                <div class="testimonials-one__client-img">
                                    <img src="assets/images/testimonial/testim1.jpg" alt="">
                                </div>
                                <div class="testimonials-one__quote"></div>
                            </div>
                            <!--Testimonials One Single-->
                            <div class="testimonials-one__single">
                                <p class="testimonials-one__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic numquam perspiciatis labore quisquam consequatur natus.</p>
                                <div class="testimonials-one__client-info">
                                    <h5 class="testimonials-one__client-name">NSIA BANQUE</h5>
                                    <p class="testimonials-one__client-title">Client</p>
                                </div>
                                <div class="testimonials-one__client-img">
                                    <img src="assets/images/testimonial/testim2.jpg" alt="">
                                </div>
                                <div class="testimonials-one__quote"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("assets/includes/cookie.php")   ?>
    <!--Site Footer One Start-->
    <?php include("assets/includes/footer.php")   ?>
    <!--Site cookie One Start-->

    <!--Site Footer One End-->

    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="#" aria-label="logo image"><img src="assets/images/AKILI_Logo Officiel[blanc].png" width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:mail.com>mail.com</a>
                </li>
                <li>
                    <i class=" fa fa-phone-alt"></i>
                        <a href="tel:225 000 000 0000">225 000 000 0000</a>
                </li>
            </ul><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter" style="text-decoration: none; color:white"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>

    <!-- /.mobile-nav__wrapper -->
    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>


</body>
<!-- /.search-popup -->
<a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
<script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendors/jarallax/jarallax.min.js"></script>
<script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
<script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
<script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
<script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="assets/vendors/nouislider/nouislider.min.js"></script>
<script src="assets/vendors/odometer/odometer.min.js"></script>
<script src="assets/vendors/swiper/swiper.min.js"></script>
<script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
<script src="assets/vendors/wnumb/wNumb.min.js"></script>
<script src="assets/vendors/wow/wow.js"></script>
<script src="assets/vendors/isotope/isotope.js"></script>
<script src="assets/vendors/countdown/countdown.min.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
<script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
<script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="sweetalert2.all.min.js"></script>
<script src="assets/owl.carousel.min.js"></script>
<script src="assets/script.js"></script>
<!-- template js -->
<script src="assets/js/aivons.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
</script>
<script>
    $('.round').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.arrow').toggleClass('bounceAlpha');
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js'></script>
<script>
    let xPos = 0;

    gsap.timeline().
    set('.ring', {
            rotationY: 180,
            cursor: 'grab'
        }) //set initial rotationY so the parallax jump happens off screen
        .set('.img', { // apply transform rotations to each image
            rotateY: i => i * -36,
            transformOrigin: '50% 50% 600px',
            z: -600,
            backfaceVisibility: 'hidden'
        }).

    from('.img', {
        duration: 1.5,
        y: 200,
        opacity: 0,
        stagger: 0.1,
        ease: 'expo'
    }).

    add(() => {
        $('.img').on('mouseenter', e => {
            let current = e.currentTarget;
            gsap.to('.img', {
                opacity: (i, t) => t == current ? 1 : 0.5,
                ease: 'power3'
            });
        });
        $('.img').on('mouseleave', e => {
            gsap.to('.img', {
                opacity: 1,
                ease: 'power2.inOut'
            });
        });
    }, '-=0.5');

    $(window).on('mousedown touchstart', dragStart);
    $(window).on('mouseup touchend', dragEnd);


    function dragStart(e) {
        if (e.touches) e.clientX = e.touches[0].clientX;
        xPos = Math.round(e.clientX);
        gsap.set('.ring', {
            cursor: 'grabbing'
        });
        $(window).on('mousemove touchmove', drag);
    }


    function drag(e) {
        if (e.touches) e.clientX = e.touches[0].clientX;

        gsap.to('.ring', {
            rotationY: '-=' + (Math.round(e.clientX) - xPos) % 360,
            onUpdate: () => {
                gsap.set('.img', {

                });
            }
        });
        xPos = Math.round(e.clientX);
    }


    function dragEnd(e) {
        $(window).off('mousemove touchmove', drag);
        gsap.set('.ring', {
            cursor: 'grab'
        });
    }


    function getBgPos(i) { //returns the background-position string to create parallax movement in each image
        return 100 - gsap.utils.wrap(0, 360, gsap.getProperty('.ring', 'rotationY') - 180 - i * 36) / 360 * 500 + 'px 0px';
    }
</script>
<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        items: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
</script>
<script>
    document.querySelector(".product-left").addEventListener("click", function() {
        Swal.fire({
            title: 'CrediRapid',
            text: "CrediRapid est une application qui permet d'effectuer des demandes de prêts bancaires en ligne ",
            imageUrl: 'assets/images/LogoCrediRapid.png',
            imageWidth: 400,
            imageHeight: 150,
            confirmButtonText: '<a href="" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
    document.querySelector(".product-middle").addEventListener("click", function() {
        Swal.fire({
            title: 'SendFund',
            text: "SendFund est une application qui permet d'envoyer de l'argent à x ",
            imageUrl: 'assets/images/Logo Sendund[noir].png',
            imageWidth: 350,
            imageHeight: 70,
            confirmButtonText: '<a href="sendFund.php" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
    document.querySelector(".product-right").addEventListener("click", function() {
        Swal.fire({
            title: 'Smart Agency',
            text: "Smart Agency est une application qui permet d'envoyer de l'argent à x ",
            imageUrl: 'assets/images/smartAgency.png',
            imageWidth: 150,
            imageHeight: 150,
            confirmButtonText: '<a href="smartAgency.php" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
</script>

</body>





</html>