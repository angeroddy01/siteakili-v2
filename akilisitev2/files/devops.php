<!DOCTYPE html>
<html lang="fr">

<!-- HEAD DEBUT -->
<?php include("assets/includes/head.php"); ?>
<!-- HEAD FIN -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header DEBUT -->
        <?php include("assets/includes/header.php");   ?>
        <!-- Header FIN -->

        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <section class="main-slider main-slider-three">
            <div class="swiper-container thm-swiper__slider" data-swiper-options='{"slidesPerView": 1, "loop": true,
            "effect": "fade",
            "pagination": {
                "el": "#main-slider-pagination",
                "type": "bullets",
                "clickable": true
            },
            "navigation": {
                "nextEl": "#main-slider__swiper-button-next",
                "prevEl": "#main-slider__swiper-button-prev"
            },
            "autoplay": {
                "delay": 5000000
            }}'>
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="image-layer" id="image-layer">
                                <img src="assets/images/auth1.jpg" alt="" srcset="">
                            </div>
                            <!-- /.image-layer -->
                            <div class="container">
                                <div class="row d-flex justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                            data-wow-duration="1500ms">
                                            <!-- <h6 class="text-center main-text">ARCHITECT SOLUTION DIGITALE</h6> -->
                                            <!-- <h4 class="text-center mt-3 fw-bold">notre équipe</h4> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        

         <section class="locations">
            <div class="container">
                <div class="location__inner">
                    <div class="section-title text-center">
                        <h2 class="section-title__title">SPECIALIST DEVELOPPEMENT – SECURITY <br> - OPERATIONS (DEVSECOPS) </h2>
                       <!--  <span class="section-title__tagline">Nous pouvons vous aider</span> -->
                    </div>
                    <div class="row justify-content-center">
                        
                        
                        <div class="col-xl-9 col-lg-9 col-md-12  wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <!--Locations Single-->
                           
                               <div class="job_description">
                                <div class="h2 text-center lh-lg">Description du Poste</div>
                                 <p class="fw-bold">Et si vous devenez un acteur majeur en matière d’accompagnement dans la transformation des Entreprises et des Administrations ? </p>
                                 <p>Construisez votre carrière dans un environnement qui fait la promotion de l’excellence professionnelle, qui place le Client et l’Employé au centre de ses préoccupations et qui promeut les Objectifs de Développement Durable (ODD)…</p>
                                 <p>Rejoignez-nous dès aujourd’hui.</p>
                                 <p>En tant que <b>Spécialiste DEVSECOPS</b>, quelles seront vos missions ?</p>
                                 
                                 
                                  <b>•</b>Travailler avec l’architecte et les autres développeurs pour traduire les besoins fonctionnels en un design technique en lien avec le schéma directeur de l’entreprise <br>
                                  <b>•</b>Développer et mettre en œuvre le logiciel de cadre DevSecOps en fonction des exigences ;  <br>
                                  <b>•</b>Mettre en place et faire évoluer le processus d’intégration continue : Validation automatique de patch<br>
                                  <b>•</b>Réaliser des Builds réguliers, avec benchmark<br>
                                  <b>•</b>Conduite de pipelines CI/CD robustes avec des capacités de sécurité et d'audit intégrées<br>
                                  <b>•</b>Estimer l'efficacité, la stabilité et l'évolutivité des ressources devops utilisées et rechercher des moyens d'améliorer ces mesures<br>
                                  <b>•</b>Mettre en place et réaliser les tests techniques, et participer à l’automatisation des test (features, non régression, perf sécurité etc…)<br>
                                  <b>•</b>Intervenir en expertise dans le cadre des opérations complexes ou sur les incidents des services en charge de l'équipe<br>
                                  <b>•</b>Organiser le support de post-production pour les systèmes<br>
                                  <b>•</b>Travailler au sein d'une équipe Agile Scrum	<br>
                                  <b>•</b>Intervenir au besoin chez nos clients en support à la maintenance ou à l’intégration de nouveaux produits
                                  <!-- <b>•</b>	 -->
                                  </p><div class="h2 text-center lh-lg">Qualifications</div>
                                 <p>Le métier de <b>Spécialiste DEVSECOPS</b> est fait pour vous si :</p>
                                 <p><b>•</b>	Vous êtes de formation supérieure Bac + 4/5 en Informatique, Génie Logiciel, Sécurité logicielle ou toutes autres formations similaires. <br>
                                 <b>•</b>	Vous justifiez d’au moins 3 années d’expérience professionnelle dont 2 ans au moins dans une fonction similaire. <br>
                                 </p>
                                
                                 <p><b>•</b>Vous avez de solides connaissance en développement d’application et des Webservices: Java, angular, nodejs, Python, Shell, Groovy, Go IC ,API SOAP et REST<br>
                                 <b>•</b>	Vous avez une bonne connaissance en SGBDR : Postgres Sql, MySQL, SQL Server, Oracle, MongoDB. <br>
                                 <b>•</b>	Vous avez de bonnes connaissance en environnements et technologies de Cloud public: Cloud Public (AWS, Azure, GCP), Cloud On-Premise, Docker, Kubernetes, Helm Chart, Terraform, Ansible, Scaleway  <br>
                                 <b>•</b>	Vous savez mettre en pratique les principes de sécurité des systèmes et réseaux : scan de vulnérabilité modernes, anti-DDOS, qualification de patchs de sécurité... <br>
                                 <b>•</b>Vous avez la capacité à collaborer au sein d’une équipe agile SCRUM	 <br>
                                 <b>•</b>Vous êtes motivés et avez une aisance relationnelle <br>
                               <!--   <b>•</b>	 <br>
                                 <b>•</b>	 <br>
                                  </p> -->
                                 <p class="fs-6"> <i class="text-danger"> <b>AVERTISSEMENT :</b> </i> La liste des responsabilités décrites de cette fiche n’est pas exhaustive et peut faire l’objet de révisions/réajustements en ligne avec l’évolution du contexte de l’entreprise</p>
                                 
                               </div>

                               
                            
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-12 wow bp p-4 fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <!--Locations Single-->
                               <div class="job_description">
                                 <h5 class="fw-semibold">Ce poste est susceptible de vous interesser ?  </h5> <br>
                                <p class="fw-light"> 
                                    Postuler dès maintenant en envoyant votre CV et une lettre de motivation à l'adresse suivante : <br>
                                    <h6 class="text-primary " style=" font-size:15px;">recrutements.akili@akilicorp.com</h6>
                                </p>
                                 <p class="fw-normal fs-6"> <i class="text-danger"> <b>NB :</b> </i> Veuillez préciser le titre du poste dans l'objet du mail</p>
                               </div>
                                
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>


  <!--  <section class="google-map mt-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7945.044296550889!2d-3.9933371000000437!3d5.336949399999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sci!4v1668770310477!5m2!1sfr!2sci" class="google-map__one" allowfullscreen></iframe>

    </section>-->
    <!--Site Footer One Start-->
    
    <?php include("assets/includes/footer.php");  ?>
    <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155"
                        alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> --><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>

    <!-- template js -->
    <script src="assets/js/aivons.js"></script>
    <!-- Changing background color -->
    <!--<script type="text/javascript">
        $(window).scroll(function() {

            // selectors
            var $window = $(window),
                $body = $('body'),
                $panel = $('.panel');

            // Change 33% earlier than scroll position so colour is there when you arrive.
            var scroll = $window.scrollTop() + ($window.height() / 3);

            $panel.each(function() {
                var $this = $(this);

                // if position is within range of this panel.
                // So position of (position of top of div <= scroll position) && (position of bottom of div > scroll position).
                // Remember we set the scroll to 33% earlier in scroll var.
                if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

                    // Remove all classes on body with color-
                    $body.removeClass(function(index, css) {
                        return (css.match(/(^|\s)color-\S+/g) || []).join(' ');
                    });

                    // Add class of currently active div
                    $body.addClass('color-' + $(this).data('color'));
                }
            });

        }).scroll();
    </script>-->
    <script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    </script>
</body>

</html>