<!DOCTYPE html>
<html lang="fr">

<!-- Head debut -->
<?php include("assets/includes/head.php"); ?>
<!--- Head Fin -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- header start --->
        <?php include("assets/includes/header.php")   ?>
        <!-- header end -->
        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <section class="main-slider">
            <!-- Swiper -->
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="assets/images/accomp.png" id="myVideo" src="assets/videos/ACCOMPAGNER, Votre transformation.mp4">       
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <!--  Texte principal -->
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                        data-wow-duration="4000ms">
                                        <h3 class="text-center main-text bcv">Accompagner</h3>
                                        <h4 class="text-center mt-3 main-text-1">Votre transformation</h4>
                                        
                                    </div>
                                    <!--  Fin texte principal -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="assets/images/deman.png" src="assets/videos/DANS UNE DEMARCHE de co-construction 2.mp4" id="myVideo">
        
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                        data-wow-duration="1500ms">
                                        <h3 class="text-center main-text">Avec une démarche</h3>
                                        <h4 class="text-center mt-3 main-text-1">de co-construction</h4>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="swiper-slide">
                        <div class="image-layer">
                            <video playsinline autoplay muted loop poster="assets/images/solution.png" src="assets/videos/DES SOLUTIONS innovantes pour votre croissance.mp4" id="myVideo">
                            </video>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                        data-wow-duration="1500ms">
                                        <h3 class="text-center main-text">Accélérer</h3>
                                        <h4 class="text-center mt-3 main-text-1">votre croissance grâce à nos solutions digitales</h4>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <!--Welcome One Start-->
        <!---->


        <section class="container mt-5 mb-5">
            <div class="row cases-row">
                <div class="section-title text-center">
                    <h2 class="section-title__title">Innovez avec le numérique</h2>
                    <span class="section-title__tagline">Nous mettons à votre disposition de puissants leviers pour accélérer votre transformation.</span>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                    <div class="panel wow fadeInUp" data-wow-delay="0ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a>
                                <img src="assets/images/2r.png">
                            </a>
                        </div>
                        <div class="bp-category-tag"><h5 class="fw-bold">L’innovation transforme l’agriculture</h5></div>
                        <div class="text-content">
                            <a href="#" class="case-title">
                                <h3> La création de champions régionaux de l’agro-industrialisation par le développement de grappes industrielles autour de chaînes de valeur prioritaires</h3>

                                <div class="real-content">
                                    <h5>
                                       Les acteurs des chaines de valeur agricole doivent identifier et surveiller les rupture leurs domaines d’activité, analyser et modéliser les impacts potentiels afin d’entrevoir l’élaboration d’une stratégie et une feuille de route de transformation

                                    </h5>
<!--  <a href="#" class="text-danger fs-6">En savoir plus >></a> -->
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 50px;">
                    <div class="panel wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a>
                                <img src="assets/images/6r.png">
                            </a>
                        </div>
                        <div class="bp-category-tag"><h5 class="fw-bold">Améliorer la vie des citoyens</h5></div>
                        <div class="text-content">
                            <a href="#" class="case-title">
                                <h3>À l’ère de l’industrie 4.0, les services publics doivent adopter le digital dans les processus pour accélérer leur modernisation</h3>

                                <div class="real-content">
                                    <h5>
                                        La gestion des processus métier (BPM) est une discipline, une technique et un outil permettant de fournir des systèmes transparents et efficaces qui desservent efficacement l’usager.
                                    </h5>
 <!-- <a href="#" class="text-danger fs-6">En savoir plus >></a> -->
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 100px;">
                    <div class="panel wow fadeInUp" data-wow-delay="600ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a href="">
                                <img src="assets/images/5r.png">
                            </a>
                        </div>
                        <div class="bp-category-tag"><h5 class="fw-bold">Services financiers en période de forte charge
</h5></div>
                        <div class="text-content">
                            <a href="#" class="case-title">
                                <h3>Les banques sont aux prises avec un volume conséquent de données</h3>

                                <div class="real-content">
                                    <h5>
                                        Les banques doivent adopter l'automatisation pour mieux accomplir les tâches en contact avec les clients
L’automatisation des process par la robotique va permettre de traiter plus rapidement le volume d’information fourni par le client tout en réduisant le taux d’erreur
                                    </h5>
 <!-- <a href="#" class="text-danger fs-6">En savoir plus >></a> -->
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 akili-cas" style="margin-top: 150px;">
                    <div class="panel wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2500ms">
                        <div class="image-placeholder">
                            <a href="">
                                <img src="assets/images/4r.png">
                            </a>
                        </div>
                        <div class="bp-category-tag"><h5 class="fw-bold">Vers une gestion lean des investissements dans le secteur minier</h5></div>
                        <div class="text-content">
                            <a href="#" class="case-title">
                                <h3> Les données analytics sont un puissant levier pour maximiser la valeur dans le secteur des mines</h3>

                                <div class="real-content">
                                    <h5>
                                        L’exploitation de la donnée par l’IA devient une priorité pour toutes les fonctions d’une organisation. 
Dans le mining, La Data associée à la la puissance de l’IA va permettre  de développer l'analytics avancé afin d’améliorer les opérations sur l'ensemble de la chaîne de valeur, de l'exploration à la production, en passant par la transformation
                                    </h5>
                                    <!-- <a href="#" class="text-danger fs-6">En savoir plus >></a> -->

                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </section>

       
     
            <section class="main-slider">
            <!-- Swiper -->
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="image-layer">
                            <img src="assets/images/auth1.jpg" alt="" srcset="" class="img-fluid">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <!--  Texte principal -->
                                    <div class="main-slider__content wow fadeInUp vvv" data-wow-delay="300ms"
                                        data-wow-duration="4000ms">
                                        
                                    </div>
                                    <h3 class="text-center fs-1 main-text-1 text-white goo1" style="text-shadow: 1.5px 1.5px #2F358B;">Voulez-vous façonner l’industrie du numérique, </h3>
                                        <h4 class="text-center mt-3 main-text-1 mb-3 text-white goo2" style="text-shadow: 1.5px 1.5px #2F358B;">Rejoignez-nous!</h4>
                                         <div class="d-flex justify-content-center goo3"><button type="button" class=" btn-lg " style="border-radius:20px;background:#2F358B  ; color:white"><a href="index3.php" style="background:#2F358B ; color:white">Postulez maintenant</a></button></div><!-- index3.php -->
                                    <!--  Fin texte principal -->
                                </div>
                            </div>
                        </div>
                    </div>
                   

                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
          
        
        <!--Cases One End-->
        <section class="cases-one cases-page mt-5">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="section-title text-center">
                        <h2 class="section-title__title">Cas clients</h2>
                       <!--  <span class="section-title__tagline"> Les produits développés par Akili et utilisés par nos
                            clients </span> -->
                    </div>
                    <?php include("assets/includes/carousel.php")   ?>
                  
                </div>

            </div>
        </section>

      


        

       <?php include("assets/includes/pay.php")   ?> 

        <!--Brand One Start-->
        <section class="brand-one mb-5">
            <div class="container text-center">
                <h2 class="section-title__title text-center mb-3">Nos partenaires</h2>
                <span class="section-title__tagline justify-content-center mb-5"> Ils nous ont déjà fait confiance.</span>


                <div class="thm-swiper__slider swiper-container" data-swiper-options='{"spaceBetween": 100, "slidesPerView": 5, "autoplay": { "delay": 1200 }, "breakpoints": {
                    "0": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "375": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "575": {
                        "spaceBetween": 30,
                        "slidesPerView": 3
                    },
                    "767": {
                        "spaceBetween": 50,
                        "slidesPerView": 4
                    },
                    "991": {
                        "spaceBetween": 50,
                        "slidesPerView": 5
                    },
                    "1199": {
                        "spaceBetween": 100,
                        "slidesPerView": 5
                    }
                }}'>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group9.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group10.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group18.png" alt="">
                        </div><!-- /.swiper-slide -->
                       
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group30.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group26.png" alt="">
                        </div><!-- /.swiper-slide -->
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group15.png" alt="">
                        </div><!-- /.swiper-lide -->
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group23.png" alt="">
                        </div><!-- /.swiper-slide -->
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group16.png" alt="">
                        </div><!-- /.swiper-slide -->
                        
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group39.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group20.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group40.png" alt="">
                        </div><!-- /.swiper-slide -->
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group32.png" alt="">
                        </div><!-- /.swiper-slide -->
                        
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group33.png" alt="">
                        </div><!-- /.swiper-slide -->

                       
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group17.png" alt="">
                        </div><!-- /.swiper-slide -->
                         <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group38.png" alt="">
                        </div><!-- /.swiper-slide -->
                       
                       
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group19.png" alt="">
                        </div><!-- /.swiper-lide -->
                        
                        <div class="swiper-slide">
                            <img src="assets/images/partenaires/Group21.png" alt="">
                        </div><!-- /.swiper-slide -->
                        
                        
                        
                       
                        
                        
                       


                    </div>
                </div>
            </div>
        </section>




        <!--Site Footer One Start-->
        <?php include("assets/includes/footer.php")   ?>
        <!--Site Footer One End-->
    </div><!-- /.page-wrapper -->
    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="#" aria-label="logo image"><img src="assets/images/AKILI_Logo Officiel[blanc].png" width="155"
                        alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:+225-27-20-31-99-80">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> --><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter" style="text-decoration: none; color:white"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>

    <!-- /.mobile-nav__wrapper -->
    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>

    <!-- /.search-popup -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="assets/owl.carousel.min.js"></script>
	<script src="assets/script.js"></script>
    <!-- template js -->
    <script src="assets/js/aivons.js"></script>
    <script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    </script>
    <script>
    $('.round').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.arrow').toggleClass('bounceAlpha');
    });
    </script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js'></script>
    <script>
    let xPos = 0;

    gsap.timeline().
    set('.ring', {
            rotationY: 180,
            cursor: 'grab'
        }) //set initial rotationY so the parallax jump happens off screen
        .set('.img', { // apply transform rotations to each image
            rotateY: i => i * -36,
            transformOrigin: '50% 50% 600px',
            z: -600,
            backfaceVisibility: 'hidden'
        }).

    from('.img', {
        duration: 1.5,
        y: 200,
        opacity: 0,
        stagger: 0.1,
        ease: 'expo'
    }).

    add(() => {
        $('.img').on('mouseenter', e => {
            let current = e.currentTarget;
            gsap.to('.img', {
                opacity: (i, t) => t == current ? 1 : 0.5,
                ease: 'power3'
            });
        });
        $('.img').on('mouseleave', e => {
            gsap.to('.img', {
                opacity: 1,
                ease: 'power2.inOut'
            });
        });
    }, '-=0.5');

    $(window).on('mousedown touchstart', dragStart);
    $(window).on('mouseup touchend', dragEnd);


    function dragStart(e) {
        if (e.touches) e.clientX = e.touches[0].clientX;
        xPos = Math.round(e.clientX);
        gsap.set('.ring', {
            cursor: 'grabbing'
        });
        $(window).on('mousemove touchmove', drag);
    }


    function drag(e) {
        if (e.touches) e.clientX = e.touches[0].clientX;

        gsap.to('.ring', {
            rotationY: '-=' + (Math.round(e.clientX) - xPos) % 360,
            onUpdate: () => {
                gsap.set('.img', {

                });
            }
        });
        xPos = Math.round(e.clientX);
    }


    function dragEnd(e) {
        $(window).off('mousemove touchmove', drag);
        gsap.set('.ring', {
            cursor: 'grab'
        });
    }


    function getBgPos(i) { //returns the background-position string to create parallax movement in each image
        return 100 - gsap.utils.wrap(0, 360, gsap.getProperty('.ring', 'rotationY') - 180 - i * 36) / 360 * 500 +
            'px 0px';
    }
    </script>
    <script>
    $('.owl-carousel1').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        items: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
    </script>
    <script>
    document.querySelector(".product-left").addEventListener("click", function() {
        Swal.fire({
            title: 'CrediRapid',
            text: "CrediRapid est une application qui permet d'effectuer des demandes de prêts bancaires en ligne ",
            imageUrl: 'assets/images/LogoCrediRapid.png',
            imageWidth: 400,
            imageHeight: 150,
            confirmButtonText: '<a href="" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
    document.querySelector(".product-middle").addEventListener("click", function() {
        Swal.fire({
            title: 'SendFund',
            text: "SendFund est une application qui permet d'envoyer de l'argent à x ",
            imageUrl: 'assets/images/Logo Sendund[noir].png',
            imageWidth: 350,
            imageHeight: 70,
            confirmButtonText: '<a href="sendFund.php" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
    document.querySelector(".product-right").addEventListener("click", function() {
        Swal.fire({
            title: 'Smart Agency',
            text: "Smart Agency est une application qui permet d'envoyer de l'argent à x ",
            imageUrl: 'assets/images/smartAgency.png',
            imageWidth: 150,
            imageHeight: 150,
            confirmButtonText: '<a href="smartAgency.php" style="text-decoration: none; color:white"> En savoir plus </a>',
            confirmButtonColor: '#2F358B',
            imageAlt: 'Custom image',
        });
    });
    </script>
    <script>
    AOS.init({
        easing: 'ease-in-sine',
        delay: 100,
        once: 'true',
    });
    </script>
    <script>
        $(".owl-carousel").owlCarousel({
  	loop:true,
    margin:10,
    nav:true,
	autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    center: true,
    navText: [
	    "<i class='fa fa-angle-left'></i>",
	    "<i class='fa fa-angle-right'></i>"
	],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
  });
    </script>

</body>

</html>