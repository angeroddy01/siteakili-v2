<!DOCTYPE html>
<html lang="en">

<!-- head -->
<?php include("assets/includes/head.php");  ?>
<!-- head fin -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header -->
        <?php include("assets/includes/header.php"); ?>
        <!--Header fin-->

        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->

        <!--Page Header Start-->
        <section class="page-header">
            <div class="page-header__bg" style="background-image: url(assets/images/backgrounds/sendBack.jpg)"></div><!-- /.page-header__bg -->
            <div class="page-header-shape-1"></div><!-- /.page-header-shape-1 -->
            <div class="page-header-shape-2"></div><!-- /.page-header-shape-2 -->
            <div class="page-header-shape-3"></div><!-- /.page-header-shape-3 -->
            <div class="container">
                <div class="page-header__inner">
                    <ul class="thm-breadcrumb list-unstyled">
                        <li><a href="index.php">Accueil</a></li>
                        <li><span>/</span></li>
                        <li>Cas clients</li>
                    </ul>
                    <h2>SENDFUND</h2>
                </div>
            </div>
        </section>
        <!--Page Header End-->

        <!--Cases Details Start-->
        <section class="cases-details">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="cases-details__img">
                            <img src="assets/images/backgrounds/sendFund.jpg" alt="" style="mix-blend-mode:normal">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8 col-lg-7">
                        <div class="cases-details__left">
                            <div class="cases-details__content">
                                <h2 class="cases-details__title">SendFund</h2>
                                <p class="cases-details__text-1">Need something changed or is there something not quite
                                    working the way you envisaged? Is your van a little old and tired and need
                                    refreshing? Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500s, when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book. It has survived not only five centuries, but also the leap into
                                    electronic typesetting, remaining essentially unchanged.</p>
                                <p class="cases-details__text-2">Need something changed or is there something not quite
                                    working the way you envisaged? Is your van a little old and tired and need
                                    refreshing? Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500s, when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book. It has survived not only five centuries, but also the leap into
                                    electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="cases-details__right">
                            <ul class="cases-details__right-list list-unstyled">
                                <li>
                                    <p>Clients:</p>
                                    <span>Jessica Brown</span>
                                </li>
                                <li>
                                    <p>Date de lancement:</p>
                                    <span>26 March, 2021</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--Cases Details End-->

        <!--Cases One Start-->
        <section class="cases-one more-cases">
            <div class="container">
                <div class="section-title text-center">
                    <h2 class="section-title__title">Plus de cas clients</h2>
                    <span class="section-title__tagline"> Nos solutions sont conçu sur mesure pour s'adapter à vos besoins </span>
                </div>
                <div class="row d-flex justify-content-center align-items-center">

                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <!--Cases One Single-->
                        <div class="cases-one__single">
                            <div class="cases-one__img-box">
                                <div class="cases-one__img">
                                    <img src="assets/images/resources/case1.jpg" alt="">
                                </div>
                                <div class="cases-one__content">
                                    <div class="cases-one__icon">
                                        <span>
                                            <img class="icon-img" src="assets/images/plus-sign.png">
                                        </span>
                                    </div>
                                    <p class="cases-one__tagline"></p>
                                    <h2 class="cases-one__tilte"><a href="cases-details.php">Whatsapp Banking</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <!--Cases One Single-->
                        <div class="cases-one__single">
                            <div class="cases-one__img-box">
                                <div class="cases-one__img">
                                    <img src="assets/images/resources/case0.jpg" alt="">
                                </div>
                                <div class="cases-one__content">
                                    <div class="cases-one__icon">
                                        <span>
                                            <img class="icon-img" src="assets/images/plus-sign.png">
                                        </span>
                                    </div>
                                    <p class="cases-one__tagline"></p>
                                    <h2 class="cases-one__tilte"><a href="smartAgency.php">Smart Agency</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Cases One End-->

        <!--Site Footer One Start-->
        <?php include("assets/includes/footer.php");   ?>
        <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

           <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>


    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>


    <!-- template js -->
    <script src="assets/js/aivons.js"></script>



</body>

</html>