<!DOCTYPE html>
<html lang="fr">

<!-- HEAD DEBUT -->
<?php include("assets/includes/head.php"); ?>
<!-- HEAD FIN -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header DEBUT -->
        <?php include("assets/includes/header.php");   ?>
        <!-- Header FIN -->

        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <section class="main-slider main-slider-three">
            <div class="swiper-container thm-swiper__slider" data-swiper-options='{"slidesPerView": 1, "loop": true,
            "effect": "fade",
            "pagination": {
                "el": "#main-slider-pagination",
                "type": "bullets",
                "clickable": true
            },
            "navigation": {
                "nextEl": "#main-slider__swiper-button-next",
                "prevEl": "#main-slider__swiper-button-prev"
            },
            "autoplay": {
                "delay": 5000000
            }}'>
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="image-layer" id="image-layer">
                                <video playsinline autoplay muted loop poster="" id="myVideo">
                                    <source src="assets/videos/contacts.mp4" type="video/webm">
                                </video>
                            </div>
                            <!-- /.image-layer -->
                            <div class="container">
                                <div class="row d-flex justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms"
                                            data-wow-duration="1500ms">
                                            <h3 class="text-center main-text">Contactez </h3>
                                            <h4 class="text-center mt-3 fw-bold">notre équipe</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Page Header Start-->

        <!--Page Header End-->

        <!--About Start-->
       <!--  <section class="about panel" data-color="white">
            <div class=" container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="about__img-box">
                            <div class="about-img">
                                <img src="assets/images/Groupe 1@2x.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 mt-5">
                        <div class="about__right">
                            <h2 class="vision__title">Notre mission</h2>
                            
                             <p class="services-details__top-text">
                                     Notre mission est de fournir aux populations défavorisées et à celles vivant en milieu rural notamment, des services de conseils et d’ingénierie en matière de transformation numérique adaptés, abordables et accessibles à partir de leur mobile et ainsi, réduire la fracture sociale.
                                    
                                </p>
                           

                          

                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!--About End-->

        <!--  <section class="message-box">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="message-box__left">
                            <div class="section-title text-left">
                                <h2 class="section-title__title">Contacts</h2>
                                <span class="section-title__tagline"> <b>Adresse :</b> Cocody 06 BP 2440 Abidjan 06  Côte d'Ivoire </span>
                                <span class="section-title__tagline"> <b>Partenariats & Ventes  :</b>  partenariats@akilicorp.com </span>
                                <span class="section-title__tagline"> <b>Service clients :</b> service.clients@akilicorp.com </span>
                            </div>
                            <div class="message-box__social">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#" class="clr-fb"><i class="fab fa-facebook"></i></a>
                                <a href="#" class="clr-dri"><i class="fab fa-dribbble"></i></a>
                                <a href="#" class="clr-ins"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="message-box__right">
                            
                            <form action="assets/inc/sendemail.php" class="comment-one__form contact-form-validated" novalidate="novalidate">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="comment-form__input-box">
                                            <input type="text" placeholder="Votre nom" name="name">
                                            <div class="comment-form__icon">
                                                <i class="far fa-user-circle"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="comment-form__input-box">
                                            <input type="email" placeholder="Adresse e-mail" name="email">
                                            <div class="comment-form__icon">
                                                <i class="far fa-envelope"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="comment-form__input-box">
                                            <textarea name="message" placeholder="Écrire un message"></textarea>
                                            <div class="comment-form__icon contact-expert__icon-comment">
                                                <i class="far fa-comment"></i>
                                            </div>
                                        </div>
                                        <button type="submit" class="thm-btn comment-form__btn">Envoyer </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

         <section class="locations">
            <div class="container">
                <div class="location__inner">
                    <div class="section-title text-center">
                        <h2 class="section-title__title">Contactez‑nous</h2>
                        <span class="section-title__tagline">Nous répondons à vos besoins</span>
                    </div>
                    <div class="row">
                        
                        
                        <div class="col-xl-6 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <!--Locations Single-->
                            <div class="locations__single">
                                <h3 class="locations__title">Partenariats & Ventes</h3>
                                <h4 class="locations__mail-phone-box">
                                <a href="mailto:partenariats@akilicorp.com" class="locations__mail">partenariats@akilicorp.com</a>
                                </h4>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <!--Locations Single-->
                            <div class="locations__single">
                               <h3 class="locations__title">Service clients </h3>
                                <h4 class="locations__mail-phone-box">
                                <a href="mailto:service.clients@akilicorp.com" class="locations__mail"> service.clients@akilicorp.com</a>
                                </h4>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>


  <!--  <section class="google-map mt-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7945.044296550889!2d-3.9933371000000437!3d5.336949399999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sci!4v1668770310477!5m2!1sfr!2sci" class="google-map__one" allowfullscreen></iframe>

    </section>-->
    <!--Site Footer One Start-->
    
    <?php include("assets/includes/footer.php");  ?>
    <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155"
                        alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> --><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>

    <!-- template js -->
    <script src="assets/js/aivons.js"></script>
    <!-- Changing background color -->
    <!--<script type="text/javascript">
        $(window).scroll(function() {

            // selectors
            var $window = $(window),
                $body = $('body'),
                $panel = $('.panel');

            // Change 33% earlier than scroll position so colour is there when you arrive.
            var scroll = $window.scrollTop() + ($window.height() / 3);

            $panel.each(function() {
                var $this = $(this);

                // if position is within range of this panel.
                // So position of (position of top of div <= scroll position) && (position of bottom of div > scroll position).
                // Remember we set the scroll to 33% earlier in scroll var.
                if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

                    // Remove all classes on body with color-
                    $body.removeClass(function(index, css) {
                        return (css.match(/(^|\s)color-\S+/g) || []).join(' ');
                    });

                    // Add class of currently active div
                    $body.addClass('color-' + $(this).data('color'));
                }
            });

        }).scroll();
    </script>-->
    <script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    </script>
</body>

</html>