<!DOCTYPE html>
<html lang="en">

<!--- HEAD START -->
<?php include("assets/includes/head.php");   ?>
<!-- HEAD END -->

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header START -->
        <?php include("assets/includes/header.php");  ?>
        <!-- Header END -->

        <div class="stricky-header stricked-menu main-menu main-menu-one">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->


        <section class="main-slider main-slider-three">
            <div class="swiper-container thm-swiper__slider" data-swiper-options='{"slidesPerView": 1, "loop": true,
            "effect": "fade",
            "pagination": {
                "el": "#main-slider-pagination",
                "type": "bullets",
                "clickable": true
            },
            "navigation": {
                "nextEl": "#main-slider__swiper-button-next",
                "prevEl": "#main-slider__swiper-button-prev"
            },
            "autoplay": {
                "delay": 5000000
            }}'>
                <div class="swiper-wrapper">
                    <div class="swiper-slide apper">
                        <div class="image-layer" id="image-layer">
                            <video playsinline autoplay muted loop poster="" id="myVideo">
                                <source src="assets/images/future_compressed.mp4" type="video/webm">
                            </video>
                        </div>
                        <!-- /.image-layer -->
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-slider__content wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                        <h3 class="text-center main-text-joinUs aac" style="text-shadow: 1.5px 1.5px #2F358B;">Voulez-vous façonner l'industrie du numérique et de l'innovation ?</h3>
                                        <h6 class="text-center mt-3  aab" style="text-shadow: 1.5px 1.5px #2F358B;">Rejoignez une entreprise de services numériques internationale au sein de laquelle vous pourrez exprimer votre créativité pour le développement de l’innovation.</h6>
                                        <div class="select-container">
                                            <h5 class="mt-3 mb-3">Recherchez un métier en fonction du domaine</h5>
                                            <select class="form-select" name="options" id="options">
                                                <option value="" selected disabled hidden>Sélectionnez un domain d'expertise</option>
                                              <!--   <option value="1" >Conseil</option> -->
                                                <option value="2">Operations</option>
                                                <!-- <option value="3">Stratégie</option> -->
                                                <!-- <option value="4">AMOA</option> -->
                                                <!-- <option value="5">Assistance Technique</option> -->
                                                <!-- <option value="6">Amoa</option> -->

                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Feature Start-->
       
        <section class="feature" id="2" style="display: none;">
            <div class="feature-bg"></div>
            <div class="container">
                <h1 class="section-title__title" style="color: black; margin-bottom:20px; text-align:center;"> Operations</h1>
                <p class="lh-5 pb-5" style="text-align: center;">Trouvez votre futur job parmi nos postes ouverts

                </p>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!--Feature Single-->
                        <div class="feature__single">
                            <div class="feature__content">
                                <h3 class="feature__title"><a href="chefdeprojetd.php"> CHEF DE PROJET DIGITAL </a></h3>
                                <!-- <p class="feature__text">la mission  de l'Ingénieur Développement Digital est d’accompagner la définition des spécifications techniques des projets,
                                   <br> Assurer le développement du code suivant son périmètre d’intervention ,
                                   <br>  Contribuer à la maintenance de  l’ensemble applicatif suivant son expertise .
                                </p> -->
                                <h3 class="feature__title fs-6 "><a href="chefdeprojetd.php"> Voir la fiche de poste </a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <!--Feature Single-->
                        <div class="feature__single">
                            <div class="feature__content">
                                <h3 class="feature__title"><a href="devops.php"> INGENIEUR DEVELOPPEMENT – SECURITY - OPERATIONS (DEVSECOPS)</a></h3>
                               <!--  <p class="feature__text">Accompagner la définition des spécifications techniques des projets, <br>
                                    Déployer l’approche DevSecOps sur tous les projets de développement ,<br>
                                    Assurer la disponibilité de l’ensemble applicatif à sa charge telle est la mission de l'Ingénieur DEVSECOPS</p> -->
                                    <h3 class="feature__title fs-6 "><a href="devops.php"> Voir la fiche de poste </a></h3>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <!--Feature Single-->
                        <div class="feature__single">
                            <div class="feature__content">
                                <h3 class="feature__title"><a href="ars.php"> ARCHITECTE SOLUTION DIGITALE</a></h3>
                               <!--  <p class="feature__text">Accompagner la définition des spécifications techniques des projets, <br>
                                    Déployer l’approche DevSecOps sur tous les projets de développement ,<br>
                                    Assurer la disponibilité de l’ensemble applicatif à sa charge telle est la mission de l'Ingénieur DEVSECOPS</p> -->
                                    <h3 class="feature__title fs-6 "><a href="ars.php"> Voir la fiche de poste </a></h3>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <!--Feature Single-->
                        <div class="feature__single">
                            <div class="feature__content">
                                <h3 class="feature__title"><a href="dataiaspecialist.php"> DATA & IA SPECIALIST </a></h3>
                               <!--  <p class="feature__text">Accompagner la définition des spécifications techniques des projets, <br>
                                    Déployer l’approche DevSecOps sur tous les projets de développement ,<br>
                                    Assurer la disponibilité de l’ensemble applicatif à sa charge telle est la mission de l'Ingénieur DEVSECOPS</p> -->
                                    <h3 class="feature__title fs-6 "><a href="dataiaspecialist.php"> Voir la fiche de poste </a></h3>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       
        <!--Feature End-->

        <!--Site Footer One Start-->
        <?php include("assets/includes/footer.php");  ?>
        <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->

    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

           <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>

    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
    <script>
        var imgLayer = document.getElementById("image-layer");
        document.getElementById('options').onchange = function() {
            var i = 1;
            var myDiv = document.getElementById(i);
            while (myDiv) {
                myDiv.style.display = 'none';
                myDiv = document.getElementById(++i);
            }
            document.getElementById(this.value).style.display = 'block';
            imgLayer.style.backgroundColor = "white";
        };
    </script>
    <!-- template js -->
    <script src="assets/js/aivons.js"></script>



</body>

</html>