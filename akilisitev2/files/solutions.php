<!DOCTYPE html>
<html lang="fr">

<?php
include("assets/includes/head.php");
?>

<body>
    <div class="preloader">
        <div class="preloader__image"></div><!-- /.preloader__image -->
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <!-- Header start -->
        <?php include("assets/includes/header.php"); ?>
        <!-- Header Fin -->
        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->

        <!--Page Header Start-->
        <section class="page-header">
            <div class="page-header__bg" style="background-image: url(assets/images/resources/impl-img.jpg"></div><!-- /.page-header__bg -->
            <div class="page-header-shape-1"></div><!-- /.page-header-shape-1 -->
            <div class="page-header-shape-2"></div><!-- /.page-header-shape-2 -->
            <div class="page-header-shape-3"></div><!-- /.page-header-shape-3 -->
            <div class="container">
                <div class="page-header__inner">
                    <h2>Implémentation des solutions</h2>
                </div>
            </div>
        </section>
        <!--Page Header End-->

        <!--News One Start-->
        <section class="news-one news-one__page">
            <div class="container">
                <div class="d-flex justify-content-center row">
                    <p class="text-center">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, quasi quam incidunt dolore labore tenetur at aliquam suscipit obcaecati accusantium expedita, dolorem vero maiores. Aliquam odit doloribus natus. Odio, ab.
                        Hic officiis vel laboriosam molestiae ipsum, odit quae magnam perferendis magni ab unde id ut vero at eaque, iusto dolor eligendi deleniti. Eligendi fugit temporibus sint iste voluptate. Odio, minus!
                        Pariatur error nam sunt, tenetur at quidem recusandae eaque odio molestias veritatis quod reprehenderit fugit temporibus aliquid ullam ex nemo laborum est, animi minus sequi dolor. At nulla eius nisi!
                    </p>
                    <div class="section-title text-center">
                        <h2 class="section-title__title x">Nos services</h2>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!--News One Single-->
                        <div class="news-one__single">
                            <div class="news-one__img">
                                <img src="assets/images/resources/data.jpg" alt="">
                                <a href="Data.php">
                                    <span class="news-one__plus"></span>
                                </a>
                            </div>
                            <div class="news-one__content">
                                <h3 class="news-one__title">
                                    <a href="Data.php">Data & Analytics</a>
                                </h3>
                                <p class="news-one__text">Aellentesque porttitor lacus quis enim varius sed efficitur
                                    turpis gilla sed sit amet.</p>
                                <a href="Data.php" class="news-one__btn"> En savoir plus </a>
                                <div class="news-one__date-box">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <!--News One Single-->
                        <div class="news-one__single">
                            <div class="news-one__img">
                                <img src="assets/images/resources/dev-img.jpg" alt="">
                                <a href="metier.php">
                                    <span class="news-one__plus"></span>
                                </a>
                            </div>
                            <div class="news-one__content">
                                <h3 class="news-one__title">
                                    <a href="metier.php"> Développement des solutions </a>
                                </h3>
                                <p class="news-one__text">Aellentesque porttitor lacus quis enim varius sed efficitur
                                    turpis gilla sed sit amet.</p>
                                <a href="metier.php" class="news-one__btn"> En savoir plus </a>
                                <div class="news-one__date-box">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--News One End-->

        <!--Site Footer One Start-->
        <?php include("assets/includes/footer.php");   ?>
        <!--Site Footer One End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="index.php" aria-label="logo image"><img src="assets/images/logo-1.png" width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

           <!-- <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:contact@akilicorp.com">contact@akilicorp.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="tel:27-20-31-99-800">tel:+225-27-20-31-99-80</a>
                </li>
            </ul> -->
            <div class="mobile-nav__top mt-2">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top mt-2 -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                    <i class="icon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="assets/vendors/odometer/odometer.min.js"></script>
    <script src="assets/vendors/swiper/swiper.min.js"></script>
    <script src="assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="assets/vendors/wow/wow.js"></script>
    <script src="assets/vendors/isotope/isotope.js"></script>
    <script src="assets/vendors/countdown/countdown.min.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendors/twentytwenty/twentytwenty.js"></script>
    <script src="assets/vendors/twentytwenty/jquery.event.move.js"></script>
    <script src="assets/vendors/bxslider/js/jquery.bxslider.min.js"></script>


    <script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>


    <!-- template js -->
    <script src="assets/js/aivons.js"></script>



</body>

</html>